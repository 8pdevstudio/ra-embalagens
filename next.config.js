/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  env: {
    API_REST: 'https://api.8pdev.studio/api/request/graph/',
    TOKEN: '784617f0-fd51-45ec-8965-e101ae6c742f',
    VUE_APP_API_URL: 'https://api.8pdev.studio',
    VUE_APP_APP_URL: 'https://app.8pdev.studio',
    VUE_APP_BOARD_URL: 'https://board.8pdev.studio',
    VUE_APP_SITE_URL: 'https://hub.8pdev.studio'
  }
}

module.exports = nextConfig
