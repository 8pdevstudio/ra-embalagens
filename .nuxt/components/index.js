export const Blog = () => import('../../components/blog.vue' /* webpackChunkName: "components/blog" */).then(c => wrapFunctional(c.default || c))
export const Contato = () => import('../../components/contato.vue' /* webpackChunkName: "components/contato" */).then(c => wrapFunctional(c.default || c))
export const Cta1 = () => import('../../components/cta1.vue' /* webpackChunkName: "components/cta1" */).then(c => wrapFunctional(c.default || c))
export const Cta2 = () => import('../../components/cta2.vue' /* webpackChunkName: "components/cta2" */).then(c => wrapFunctional(c.default || c))
export const Depoimentos = () => import('../../components/depoimentos.vue' /* webpackChunkName: "components/depoimentos" */).then(c => wrapFunctional(c.default || c))
export const Navbar = () => import('../../components/navbar.vue' /* webpackChunkName: "components/navbar" */).then(c => wrapFunctional(c.default || c))
export const Newsletter = () => import('../../components/newsletter.vue' /* webpackChunkName: "components/newsletter" */).then(c => wrapFunctional(c.default || c))
export const Numeros = () => import('../../components/numeros.vue' /* webpackChunkName: "components/numeros" */).then(c => wrapFunctional(c.default || c))
export const Parcerias = () => import('../../components/parcerias.vue' /* webpackChunkName: "components/parcerias" */).then(c => wrapFunctional(c.default || c))
export const Produtos = () => import('../../components/produtos.vue' /* webpackChunkName: "components/produtos" */).then(c => wrapFunctional(c.default || c))
export const Responsabilidades = () => import('../../components/responsabilidades.vue' /* webpackChunkName: "components/responsabilidades" */).then(c => wrapFunctional(c.default || c))
export const Rodape = () => import('../../components/rodape.vue' /* webpackChunkName: "components/rodape" */).then(c => wrapFunctional(c.default || c))
export const SobreNos = () => import('../../components/sobre-nos.vue' /* webpackChunkName: "components/sobre-nos" */).then(c => wrapFunctional(c.default || c))
export const TrabalheConosco = () => import('../../components/trabalhe-conosco.vue' /* webpackChunkName: "components/trabalhe-conosco" */).then(c => wrapFunctional(c.default || c))
export const Vitrine = () => import('../../components/vitrine.vue' /* webpackChunkName: "components/vitrine" */).then(c => wrapFunctional(c.default || c))

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
