# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Blog>` | `<blog>` (components/blog.vue)
- `<Contato>` | `<contato>` (components/contato.vue)
- `<Cta1>` | `<cta1>` (components/cta1.vue)
- `<Cta2>` | `<cta2>` (components/cta2.vue)
- `<Depoimentos>` | `<depoimentos>` (components/depoimentos.vue)
- `<Navbar>` | `<navbar>` (components/navbar.vue)
- `<Newsletter>` | `<newsletter>` (components/newsletter.vue)
- `<Numeros>` | `<numeros>` (components/numeros.vue)
- `<Parcerias>` | `<parcerias>` (components/parcerias.vue)
- `<Produtos>` | `<produtos>` (components/produtos.vue)
- `<Responsabilidades>` | `<responsabilidades>` (components/responsabilidades.vue)
- `<Rodape>` | `<rodape>` (components/rodape.vue)
- `<SobreNos>` | `<sobre-nos>` (components/sobre-nos.vue)
- `<TrabalheConosco>` | `<trabalhe-conosco>` (components/trabalhe-conosco.vue)
- `<Vitrine>` | `<vitrine>` (components/vitrine.vue)
