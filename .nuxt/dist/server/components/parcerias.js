exports.ids = [9];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/parcerias.vue?vue&type=template&id=f110764c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"py-64 py-md-144",attrs:{"id":"parcerias"}},[_vm._ssrNode("<div class=\"container-theme d-flex justify-content-center\"><div class=\"container\"><div class=\"max-w-900 mb-md-32\"><div class=\"text-danger fs-5 fw-700 mb-24\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.titulo_parcerias)+"\n        ")+"</div> <h2 class=\"font-title font-32 font-md-64 fw-700\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.chamada_parcerias)+"\n        ")+"</h2></div> <div class=\"row\">"+(_vm._ssrList((_vm.content.motivos_parcerias),function(item){return ("<div class=\"col-12 col-md-6 mt-24 mt-md-48\"><div class=\"d-flex\"><div class=\"pt-2\"><div class=\"bg-danger text-white rounded-6 p-48 position-relative\"><i"+(_vm._ssrClass(null,((item.icone_motivos_parcerias) + "  fs-3 position-absolute top-50 start-50 translate-middle")))+"></i></div></div> <div class=\"w-100 pl-16 px-md-24 pt-1 pr-lg-72\"><div class=\"font-title text-danger mb-8 font-24 fw-700\">"+_vm._ssrEscape("\n                "+_vm._s(item.titulo_motivos_parcerias)+"\n              ")+"</div> <div class=\"fs-5\">"+_vm._ssrEscape(_vm._s(item.descricao_motivos_parcerias))+"</div></div></div></div>")}))+"</div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/parcerias.vue?vue&type=template&id=f110764c&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/parcerias.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var parceriasvue_type_script_lang_js_ = ({
  name: 'Parcerias',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('parcerias');
      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/parcerias.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_parceriasvue_type_script_lang_js_ = (parceriasvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/parcerias.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_parceriasvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "c7fbef3c"
  
)

/* harmony default export */ var parcerias = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=parcerias.js.map