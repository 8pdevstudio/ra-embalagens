exports.ids = [2];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("24ab91f4", content, true, context)
};

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contato_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(63);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contato_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contato_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contato_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_3_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_3_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_3_oneOf_1_2_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contato_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ratio.maps{--bs-aspect-ratio:24%}iframe{width:100%;height:200px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/contato.vue?vue&type=template&id=6d9b96ca&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{attrs:{"id":"contato"}},[_vm._ssrNode("<div class=\"py-24 py-md-72\"><div class=\"container-theme bg-theme-light py-md-144\"><div class=\"container\"><div class=\"row justify-content-between\"><div class=\"col-md-5\"><p class=\"text-danger fs-5 fw-700 mb-16\">"+_vm._ssrEscape("\n              "+_vm._s(_vm.content.titulo_contato)+"\n            ")+"</p> <h3 class=\"font-title font-32 font-md-56 fw-700 mb-24\">"+_vm._ssrEscape("\n              "+_vm._s(_vm.content.chamada_contato)+"\n            ")+"</h3> <div class=\"fs-5 max-w-400\"><p class=\"text-secondary mb-0\">Endereço</p> <a"+(_vm._ssrAttr("href",("https://www.google.com.br/maps/search/" + (_vm.content.endereco_contato))))+" target=\"_blank\"><p class=\"text-danger mb-16 fw-700\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.content.endereco_contato)+"\n                ")+"</p></a> <p class=\"text-secondary mb-0\">Telefone</p> <a"+(_vm._ssrAttr("href",("tel:55" + (_vm.content.telefone_contato.replace(/\D/g, '')))))+"><p class=\"text-danger fw-700 mb-0\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.content.telefone_contato)+"\n                ")+"</p></a> <p class=\"text-secondary mb-16\">"+_vm._ssrEscape("\n                "+_vm._s(_vm.content.horario_contato)+"\n              ")+"</p> <p class=\"text-secondary mb-0\">Mande um e-mail</p> <a"+(_vm._ssrAttr("href",("mailto:" + (_vm.content.email_contato))))+"><p class=\"text-danger fw-700\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.content.email_contato)+"\n                ")+"</p></a></div></div> <div class=\"col-md-6\"><div class=\"row\"><div class=\"col-12\"><div class=\"mb-4\"><label class=\"fs-5 mb-8\">Nome</label> <input type=\"text\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div></div> <div class=\"col-6\"><div class=\"mb-4\"><label class=\"fs-5 mb-8\">E-mail</label> <input type=\"email\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div></div> <div class=\"col-6\"><div class=\"mb-4\"><label class=\"fs-5 mb-8\">Telefone ou WhatsApp</label> <input type=\"tel\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div></div> <div class=\"col-12\"><div class=\"mb-4\"><label class=\"fs-5 mb-8\">Assunto</label> <input type=\"text\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div></div> <div class=\"col-12\"><div class=\"mb-4\"><label class=\"fs-5 mb-8\">Sua mensagem (opcional)</label> <textarea class=\"form-control form-control-lg border-danger rounded-0 p-16\"></textarea></div></div> <div class=\"col-12\"><div class=\"mb-16\"><label class=\"d-flex\"><input required=\"required\" type=\"checkbox\" class=\"mt-1\"> <div class=\"form-check-label small pl-8 max-w-500\">"+(_vm._s(_vm.content.texto_consentimento_contato))+"</div></label></div></div> <div class=\"col-12 pt-8\"><div class=\"btn-group w-100\"><a class=\"btn btn-danger btn-lg rounded-5 pl-40 pr-112 py-24 position-relative\">"+_vm._ssrEscape("\n                    "+_vm._s(_vm.content.botao_contato)+"\n                    ")+"<div class=\"bg-dark bg-opacity-10 position-absolute top-0 end-0 px-40 d-flex align-items-center h-100\"><i class=\"far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1\"></i></div></a></div></div></div></div></div></div></div></div> <div class=\"bg-secondary position-relative\"><div class=\"ratio maps\">"+(_vm._s(_vm.content.codigo_mapa_contato))+"</div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/contato.vue?vue&type=template&id=6d9b96ca&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/contato.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var contatovue_type_script_lang_js_ = ({
  name: 'Contato',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('contato'); // console.log(content)

      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/contato.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_contatovue_type_script_lang_js_ = (contatovue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/contato.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(71)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_contatovue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "ba5c51c0"
  
)

/* harmony default export */ var contato = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=contato.js.map