exports.ids = [10];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("1b04971c", content, true, context)
};

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_produtos_vue_vue_type_style_index_0_id_3142f2b2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(66);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_produtos_vue_vue_type_style_index_0_id_3142f2b2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_produtos_vue_vue_type_style_index_0_id_3142f2b2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_produtos_vue_vue_type_style_index_0_id_3142f2b2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_produtos_vue_vue_type_style_index_0_id_3142f2b2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".h-250[data-v-3142f2b2]{height:250px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/produtos.vue?vue&type=template&id=3142f2b2&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-theme bg-theme-light position-relative z-index-1",attrs:{"id":"produtos"}},[_vm._ssrNode("<div class=\"container py-144\" data-v-3142f2b2><div class=\"row\" data-v-3142f2b2><div class=\"col\" data-v-3142f2b2><h2 class=\"font-title fw-700 font-32 font-md-56 text-center mb-64\" data-v-3142f2b2>"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.titulo_produtos)+"\n        ")+"</h2></div></div> <div class=\"row\" data-v-3142f2b2>"+(_vm._ssrList((_vm.content.sublista_produtos),function(card){return ("<div class=\"col-md-3\" data-v-3142f2b2><div class=\"rounded-6 bg-white position-relative h-100 overflow-hidden\" data-v-3142f2b2><div class=\"hover-effect\" data-v-3142f2b2><div class=\"ratio ratio-4x3 position-relative\" data-v-3142f2b2><img"+(_vm._ssrAttr("src",_vm.getImage(card.imagem_sublista_produtos, 'sm')))+" class=\"bg-image\" data-v-3142f2b2></div> <div class=\"p-16 p-md-4\" data-v-3142f2b2><div class=\"rounded-circle d-inline-block bg-danger w-auto text-white p-16 position-relative\" data-v-3142f2b2><div class=\"p-1\" data-v-3142f2b2></div> <i"+(_vm._ssrClass(null,((card.icone_sublista_produtos) + " fs-4 position-absolute top-50 start-50 translate-middle")))+" data-v-3142f2b2></i></div> <div class=\"fw-800 font-md-24 mt-8 lh-h w-70\" data-v-3142f2b2>"+_vm._ssrEscape("\n                "+_vm._s(card.titulo_sublista_produtos)+"\n              ")+"</div></div> <div class=\"position-absolute top-0 start-0 w-100 h-100 bg-danger rounded-3 hover-effect-target p-16\" data-v-3142f2b2><div class=\"position-relative m-8 h-100 pb-56\" data-v-3142f2b2><div class=\"rounded-circle d-inline-block bg-white w-auto text-white p-16 position-relative\" data-v-3142f2b2><div class=\"p-1\" data-v-3142f2b2></div> <i"+(_vm._ssrClass(null,((card.icone_sublista_produtos) + " text-danger fs-4 position-absolute top-50 start-50 translate-middle")))+" data-v-3142f2b2></i></div> <div class=\"font-title fw-600 mt-16 text-white font-16 font-md-24 lh-h mb-8\" data-v-3142f2b2>"+_vm._ssrEscape("\n                  "+_vm._s(card.titulo_sublista_produtos)+"\n                ")+"</div> <div class=\"lh-body text-white\" data-v-3142f2b2>"+_vm._ssrEscape("\n                  "+_vm._s(card.descricao_sublista_produtos)+"\n                ")+"</div> <div class=\"position-absolute w-100 bottom-0 pb-16 start-0\" data-v-3142f2b2><a href=\"#\" class=\"text-light\" data-v-3142f2b2> Veja nosso catalogo </a></div></div></div></div></div></div>")}))+"</div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/produtos.vue?vue&type=template&id=3142f2b2&scoped=true&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/produtos.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var produtosvue_type_script_lang_js_ = ({
  name: 'Produtos',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('produtos'); // console.log(content, '<<<')

      return content;
    },

    produtos() {
      return typeof this.content !== 'undefined' && this.content.sublista_produtos ? this.content.sublista_produtos : [];
    }

  }
});
// CONCATENATED MODULE: ./components/produtos.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_produtosvue_type_script_lang_js_ = (produtosvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/produtos.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(77)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_produtosvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "3142f2b2",
  "a93ba2d8"
  
)

/* harmony default export */ var produtos = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=produtos.js.map