exports.ids = [6];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/navbar.vue?vue&type=template&id=575dd42c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.content)?_c('div',{staticClass:"container-theme py-24"},[_vm._ssrNode("<div class=\"row align-items-center py-8\"><div class=\"col\">"+((_vm.getImage(_vm.content.config.logo_configuracao, 'sm'))?("<img"+(_vm._ssrAttr("src",("" + (_vm.getImage(_vm.content.config.logo_configuracao, 'sm')))))+(_vm._ssrAttr("title",_vm.content.title))+" width=\"120\">"):"<!---->")+"</div> <div class=\"col-auto\"><ul class=\"nav\">"+(_vm._ssrList((_vm.links),function(link){return ("<li class=\"nav-item mx-8\"><a"+(_vm._ssrAttr("href",("" + (link.href))))+" class=\"nav-link link-dark fw-600 text-uppercase px-8 fs-6\">"+_vm._ssrEscape("\n            "+_vm._s(link.name)+"\n          ")+"</a></li>")}))+"</ul></div> <div class=\"col-auto pl-md-24\"><a href=\"#contato\" class=\"btn btn-danger px-32 py-16 text-uppercase font-16\">contato</a></div></div>")]):_vm._e()}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/navbar.vue?vue&type=template&id=575dd42c&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/navbar.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var navbarvue_type_script_lang_js_ = ({
  name: 'Navbar',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {
      links: [{
        id: 1,
        name: 'sobre nós',
        href: '#sobre'
      }, {
        id: 2,
        name: 'produtos',
        href: '#produtos'
      }, {
        id: 3,
        name: 'responsabilidade socioambiental',
        href: '#responsabilidades'
      }, {
        id: 4,
        name: 'blog',
        href: 'blog'
      }]
    };
  },

  computed: {
    content() {
      const content = this.getContent('vitrine');
      content.config = this.getContent('configuracao');
      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/navbar.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_navbarvue_type_script_lang_js_ = (navbarvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/navbar.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_navbarvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "18953f8a"
  
)

/* harmony default export */ var navbar = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=navbar.js.map