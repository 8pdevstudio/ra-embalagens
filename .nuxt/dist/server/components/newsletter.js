exports.ids = [7];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/newsletter.vue?vue&type=template&id=7ab56862&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid bg-theme-light pt-16",attrs:{"id":"newsletter"}},[_vm._ssrNode("<div class=\"container py-64\"><div class=\"text-danger fs-5 fw-700 mb-16\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.content.titulo_detalhes_newsletter)+"\n    ")+"</div> <h2 class=\"font-title font-32 fw-700 pb-4\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.content.chamada_detalhes_newsletter)+"\n    ")+"</h2> <div class=\"row gx-3 mb-16\"><div class=\"col\"><label class=\"fs-5 mb-8\">Nome</label> <input type=\"text\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div> <div class=\"col\"><label class=\"fs-5 mb-8\">Email</label> <input type=\"email\" class=\"form-control form-control-lg border-danger rounded-0 p-16\"></div> <div class=\"col-auto pt-40\"><button type=\"button\" class=\"btn btn-danger px-32 h-100\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.botao_detalhes_newsletter ? _vm.content.botao_detalhes_newsletter : 'Assinar')+"\n        ")+"</button></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/newsletter.vue?vue&type=template&id=7ab56862&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/newsletter.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var newslettervue_type_script_lang_js_ = ({
  name: 'Newsletter',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('detalhes_newsletter'); // console.log(content, '<<')

      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/newsletter.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_newslettervue_type_script_lang_js_ = (newslettervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/newsletter.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_newslettervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "754f2a9a"
  
)

/* harmony default export */ var newsletter = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=newsletter.js.map