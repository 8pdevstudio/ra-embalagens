exports.ids = [8];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/numeros.vue?vue&type=template&id=9546e838&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"py-64 py-md-144",attrs:{"id":"numeros"}},[_vm._ssrNode("<div class=\"container\"><p class=\"text-danger fs-5 fw-700 mb-24\">"+_vm._ssrEscape("\n      "+_vm._s(_vm.content.titulo_numeros)+"\n    ")+"</p> <div class=\"row align-items-center gx-5 mb-64\"><div class=\"col-md-7\"><h2 class=\"font-title font-32 font-md-64 fw-700 mb-48\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.chamada_numeros)+"\n        ")+"</h2></div> <div class=\"col-md-5\"><p class=\"text-secondary font-16 font-md-24\">"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.descricao_numeros)+"\n        ")+"</p></div></div> <div class=\"row\">"+(_vm._ssrList((_vm.content.lista_numeros),function(item){return ("<div class=\"col-md-3 d-flex justify-content-center\"><div class=\"position-relative\"><div class=\"p-56 bg-danger position-absolute rounded-circle top-0 start-0 mt--8 ml--48\"></div> <div class=\"position-relative z-index-1\"><div class=\"font-80 font-md-96 fw-600 lh-h\">"+_vm._ssrEscape("\n              "+_vm._s(item.numero_lista_numeros)+"\n            ")+"</div> <div class=\"lh-body font-16 font-md-24 fw-bold max-w-200\">"+_vm._ssrEscape("\n              "+_vm._s(item.descricao_lista_numeros)+"\n            ")+"</div></div></div></div>")}))+"</div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/numeros.vue?vue&type=template&id=9546e838&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/numeros.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var numerosvue_type_script_lang_js_ = ({
  name: 'Numeros',
  mixins: [contentMixin["a" /* default */]],
  computed: {
    content() {
      const content = this.getContent('numeros');
      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/numeros.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_numerosvue_type_script_lang_js_ = (numerosvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/numeros.vue





/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_numerosvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  "da3ba366"
  
)

/* harmony default export */ var numeros = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=numeros.js.map