exports.ids = [4];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("a2422592", content, true, context)
};

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cta2_vue_vue_type_style_index_0_id_76b8c8cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(65);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cta2_vue_vue_type_style_index_0_id_76b8c8cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cta2_vue_vue_type_style_index_0_id_76b8c8cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cta2_vue_vue_type_style_index_0_id_76b8c8cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cta2_vue_vue_type_style_index_0_id_76b8c8cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".ear[data-v-76b8c8cf]{width:5rem;height:7.5rem;border-top:.9rem solid #fff;border-right:.9rem solid #fff}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/cta2.vue?vue&type=template&id=76b8c8cf&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-fluid position-relative mt--144 bg-danger"},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",("" + (_vm.getImage(_vm.content.imagem_fundo_cta2, 'lg')))))+" class=\"bg-image\" data-v-76b8c8cf> <div class=\"position-absolute top-0 start-0 w-100 h-100 bg-dark bg-opacity-75\" data-v-76b8c8cf></div> <div class=\"container pt-144\" data-v-76b8c8cf><div class=\"z-index-1 w-100 py-144 d-flex justify-content-center\" data-v-76b8c8cf><div class=\"position-relative px-144\" data-v-76b8c8cf><h2 class=\"font-title font-40 font-md-88 text-white fw-700\" data-v-76b8c8cf>"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.chamada_cta2)+"\n        ")+"</h2> <a"+(_vm._ssrAttr("href",("" + (_vm.content.link_cta2))))+" class=\"text-dark\" data-v-76b8c8cf><div class=\"bg-white rounded-7 p-16 w-fit mt-32\" data-v-76b8c8cf><div class=\"row gx-3 align-items-center\" data-v-76b8c8cf><div class=\"col-auto\" data-v-76b8c8cf><div class=\"bg-dark p-40 rounded-6 text-dark position-relative overflow-hidden\" data-v-76b8c8cf><img"+(_vm._ssrAttr("src",("" + (_vm.getImage(
                      _vm.content.miniatura_botao_cta2,
                      'thumb'
                    )))))+" class=\"bg-image\" data-v-76b8c8cf></div></div> <div class=\"col\" data-v-76b8c8cf><div class=\"lh-h\" data-v-76b8c8cf><div class=\"mb-0 font-24 fw-600\" data-v-76b8c8cf>"+_vm._ssrEscape("\n                    "+_vm._s(_vm.content.titulo_botao_cta2)+"\n                  ")+"</div> <div class=\"text-muted\" data-v-76b8c8cf>"+_vm._ssrEscape("\n                    "+_vm._s(_vm.content.descricao_botao_cta2)+"\n                  ")+"</div></div></div> <div class=\"col-auto\" data-v-76b8c8cf><div class=\"bg-danger p-40 rounded-6 text-white position-relative overflow-hidden\" data-v-76b8c8cf><i"+(_vm._ssrClass(null,((_vm.content.icone_botao_cta2) + " font-32 position-absolute top-50 start-50 translate-middle")))+" data-v-76b8c8cf></i></div></div></div></div></a> <div class=\"ear position-absolute top-0 end-0\" data-v-76b8c8cf></div></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/cta2.vue?vue&type=template&id=76b8c8cf&scoped=true&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/cta2.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var cta2vue_type_script_lang_js_ = ({
  name: 'Cta2',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('cta2');
      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/cta2.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_cta2vue_type_script_lang_js_ = (cta2vue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/cta2.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(75)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_cta2vue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "76b8c8cf",
  "74195b1c"
  
)

/* harmony default export */ var cta2 = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=cta2.js.map