exports.ids = [12];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/rodape.vue?vue&type=template&id=cc264c80&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container-theme py-64 pt-md-96 px-md-144"},[_vm._ssrNode("<div class=\"row align-items-end\"><div class=\"col-8\">"+((_vm.getImage(_vm.content.config.logo_configuracao, 'sm'))?("<img"+(_vm._ssrAttr("src",("" + (_vm.getImage(_vm.content.config.logo_configuracao, 'sm')))))+" title=\"Logo\" class=\"max-w-250 mb-24 mb-md-32\">"):"<!---->")+" <div class=\"text-secondary max-w-300 mb-32 mb-md-48\">"+_vm._ssrEscape("\n        "+_vm._s(_vm.content.config.descricao_configuracao)+"\n      ")+"</div> <ul class=\"nav mt-24\">"+(_vm._ssrList((_vm.links),function(link){return ("<li class=\"nav-item mx-8\"><a"+(_vm._ssrAttr("href",("" + (link.href))))+" class=\"nav-link link-dark fw-600 text-uppercase pe-8 ps-0 fs-6\">"+_vm._ssrEscape("\n            "+_vm._s(link.name)+"\n          ")+"</a></li>")}))+"</ul></div> <div class=\"col-md-4 pl-md-40\"><h3 class=\"font-title text-danger fw-700 font-32\">"+_vm._ssrEscape("\n        "+_vm._s(_vm.content.titulo_contato)+"\n      ")+"</h3> <a"+(_vm._ssrAttr("href",("" + (_vm.content.whatsapp_texto_botao_contato.text))))+(_vm._ssrAttr("target",("" + (_vm.content.whatsapp_texto_botao_contato.tab))))+"><div class=\"bg-light rounded-7 p-16 w-fit mt-16 mb-24\"><div class=\"row gx-3 align-items-center\"><div class=\"col-auto\"><div class=\"bg-secondary p-40 rounded-6 text-dark position-relative overflow-hidden\"><img"+(_vm._ssrAttr("src",("" + (_vm.getImage(
                    _vm.content.miniatura_whatsapp_contato,
                    'sm'
                  )))))+" class=\"bg-image\"></div></div> <div class=\"col\"><div class=\"lh-body\"><div class=\"mb-0 fs-5 lh-h fw-600 text-dark\">"+_vm._ssrEscape("\n                  "+_vm._s(_vm.content.whatsapp_texto_botao_contato.label)+"\n                ")+"</div></div></div> <div class=\"col-auto\"><div class=\"bg-danger p-40 rounded-6 text-white position-relative overflow-hidden\"><i"+(_vm._ssrClass(null,"fab fa-whatsapp font-32 position-absolute top-50 start-50 translate-middle"))+"></i></div></div></div></div></a> <div class=\"max-w-400\"><div class=\"row gx-3 mt-16\"><div class=\"col-auto\"><div><i class=\"fa fa-phone text-danger\"></i></div></div> <div class=\"col\"><a"+(_vm._ssrAttr("href",("tel:55" + (_vm.content.telefone_contato.replace(/\D/g, '')))))+" class=\"text-dark\"><p class=\"mb-0\">"+_vm._ssrEscape("Telefone "+_vm._s(_vm.content.telefone_contato))+"</p></a></div></div> <div class=\"row gx-3 mt-16\"><div class=\"col-auto\"><div><i class=\"fa fa-envelope text-danger\"></i></div></div> <div class=\"col\"><a"+(_vm._ssrAttr("href",("mailto:" + (_vm.content.email_contato))))+" class=\"text-dark\"><p class=\"mb-0\">"+_vm._ssrEscape(_vm._s(_vm.content.email_contato))+"</p></a></div></div> <div class=\"row gx-3 mt-16\"><div class=\"col-auto\"><div><i class=\"fa fa-map-marker-alt text-danger\"></i></div></div> <div class=\"col\"><a"+(_vm._ssrAttr("href",("https://www.google.com.br/maps/search/" + (_vm.content.endereco_contato))))+" target=\"_blank\" class=\"text-dark\"><p class=\"mb-0\">"+_vm._ssrEscape(_vm._s(_vm.content.endereco_contato))+"</p></a></div></div> <div class=\"mt-16 mb-0 text-secondary font-16\">"+(_vm._s(_vm.content.horario_contato))+"</div></div></div></div> <hr class=\"my-32 my-md-40\"> <div class=\"row align-items-center\"><div class=\"col\"><a href=\"#\" class=\"link-secondary decoration-none font-16 fw-400\">Termos / Politica de privacidade</a></div> <div class=\"col-auto\"><p class=\"mb-0 text-secondary font-16 fw-400\">"+_vm._ssrEscape("\n        © Copyright "+_vm._s(new Date().getFullYear())+"\n      ")+"</p></div> <div class=\"col\"><div class=\"d-flex justify-content-end\">"+((_vm.content.facebook_contato)?("<a"+(_vm._ssrAttr("href",("" + (_vm.content.facebook_contato))))+" target=\"_blank\" class=\"border-danger p-4 mx-1 border-1 rounded-circle position-relative border\"><i class=\"fab fa-facebook-f text-danger font-24 position-absolute top-50 start-50 translate-middle\"></i></a>"):"<!---->")+" "+((_vm.content.instagram_contato)?("<a"+(_vm._ssrAttr("href",("" + (_vm.content.instagram_contato))))+" target=\"_blank\" class=\"border-danger p-4 mx-1 border-1 rounded-circle position-relative border\"><i class=\"fab fa-instagram text-danger font-24 position-absolute top-50 start-50 translate-middle\"></i></a>"):"<!---->")+" "+((_vm.content.linkedin_contato)?("<a"+(_vm._ssrAttr("href",("" + (_vm.content.linkedin_contato))))+" target=\"_blank\" class=\"border-danger p-4 mx-1 border-1 rounded-circle position-relative border\"><i class=\"fab fa-linkedin text-danger font-24 position-absolute top-50 start-50 translate-middle\"></i></a>"):"<!---->")+" "+((_vm.content.twitter_contato)?("<a"+(_vm._ssrAttr("href",("" + (_vm.content.twitter_contato))))+" target=\"_blank\" class=\"border-danger p-4 mx-1 border-1 rounded-circle position-relative border\"><i class=\"fab fa-twitter text-danger font-24 position-absolute top-50 start-50 translate-middle\"></i></a>"):"<!---->")+" "+((_vm.content.youtube_contato)?("<a"+(_vm._ssrAttr("href",("" + (_vm.content.youtube_contato))))+" target=\"_blank\" class=\"border-danger p-4 mx-1 border-1 rounded-circle position-relative border\"><i class=\"fab fa-youtube text-danger font-24 position-absolute top-50 start-50 translate-middle\"></i></a>"):"<!---->")+"</div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/rodape.vue?vue&type=template&id=cc264c80&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/rodape.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var rodapevue_type_script_lang_js_ = ({
  name: 'Rodape',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {
      links: [{
        id: 1,
        name: 'sobre nós',
        href: '#sobre'
      }, {
        id: 2,
        name: 'produtos',
        href: '#produtos'
      }, {
        id: 3,
        name: 'responsabilidade socioambiental',
        href: '#responsabilidades'
      }, {
        id: 4,
        name: 'blog',
        href: 'blog'
      }]
    };
  },

  computed: {
    content() {
      const content = this.getContent('contato');
      content.config = this.getContent('configuracao'); // console.log(content, '<<')

      return content;
    }

  }
});
// CONCATENATED MODULE: ./components/rodape.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_rodapevue_type_script_lang_js_ = (rodapevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/rodape.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_rodapevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "c8c19a2e"
  
)

/* harmony default export */ var rodape = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=rodape.js.map