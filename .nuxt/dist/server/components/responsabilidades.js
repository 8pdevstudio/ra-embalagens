exports.ids = [11];
exports.modules = {

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuex__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(57);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["a"] = ({
  computed: { ...Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('content', ['allContent'])
  },
  methods: {
    getContent(area = null, path = null) {
      if (area) {
        if (path) {
          return lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.allContent[area], path);
        }

        return this.allContent[area];
      }

      return this.allContent;
    },

    getImage(image, size = 'md') {
      if (image !== null && image !== undefined) {
        return image[0] && image[0][size] ? image[0][size] : false;
      }

      return false;
    }

  }
});

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(80);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(6).default
module.exports.__inject__ = function (context) {
  add("4e7a6c16", content, true, context)
};

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_responsabilidades_vue_vue_type_style_index_0_id_1939e84f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(67);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_responsabilidades_vue_vue_type_style_index_0_id_1939e84f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_responsabilidades_vue_vue_type_style_index_0_id_1939e84f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_responsabilidades_vue_vue_type_style_index_0_id_1939e84f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_responsabilidades_vue_vue_type_style_index_0_id_1939e84f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(5);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "ul .nav-link.active[data-v-1939e84f],ul .nav-link.active~.nav-link[data-v-1939e84f]{color:red!important}ul .nav-link.active>div[data-v-1939e84f]:first-child,ul .nav-link.active~.nav-link>div[data-v-1939e84f]:first-child{position:relative}ul .nav-link.active>div[data-v-1939e84f]:first-child:before,ul .nav-link.active~.nav-link>div[data-v-1939e84f]:first-child:before{content:\"\";position:absolute;top:13px;left:-30px;width:10px;height:10px;border-radius:50%;background-color:red}ul .nav-link.active .text-content[data-v-1939e84f],ul .nav-link.active~.nav-link .text-content[data-v-1939e84f]{color:var(--bs-dark)!important}ul .nav-link.active.border-secondary[data-v-1939e84f],ul .nav-link.active~.nav-link.border-secondary[data-v-1939e84f]{border-color:red!important}.ear[data-v-1939e84f]{width:5rem;height:7.5rem;border-top:.9rem solid #fff;border-right:.9rem solid #fff}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/responsabilidades.vue?vue&type=template&id=1939e84f&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{staticClass:"container-theme bg-theme-light py-64 py-md-144 position-relative z-index-1 d-flex align-items-center flex-column",attrs:{"id":"responsabilidades"}},[_vm._ssrNode("<div class=\"container\" data-v-1939e84f><div class=\"row align-items-center\" data-v-1939e84f><div class=\"col-12\" data-v-1939e84f><div class=\"text-danger fs-5 fw-700 mb-24\" data-v-1939e84f>"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.titulo_responsabilidades)+"\n        ")+"</div> <h2 class=\"font-title font-32 font-md-56 fw-700 max-w-450 mb-md-32\" data-v-1939e84f>"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.chamada_responsabilidades)+"\n        ")+"</h2></div> <div class=\"col-md-6\" data-v-1939e84f><div class=\"text-secondary font-24 mb-64 max-w-450\" data-v-1939e84f>"+_vm._ssrEscape("\n          "+_vm._s(_vm.content.descricao_responsabilidades)+"\n        ")+"</div> <ul id=\"tab-items\" role=\"tablist\" class=\"nav ps-0 max-w-450\" data-v-1939e84f>"+(_vm._ssrList((_vm.content.lista_responsabilidades),function(item,index){return ("<li role=\"presentation\" class=\"nav-item mb-24 mb-md-40 fs-5\" data-v-1939e84f><a href=\"javascript:;\""+(_vm._ssrAttr("id",("tab-" + (item.id))))+" data-bs-toggle=\"tab\""+(_vm._ssrAttr("data-bs-target",("#item-" + (item.id))))+" role=\"tab\""+(_vm._ssrAttr("aria-controls",("item-" + (item.id))))+" aria-selected=\"false\""+(_vm._ssrClass("nav-link tab ps-0 text-secondary",{ active: index === 0 }))+" data-v-1939e84f><div class=\"font-title fw-700 mb-8 font-24\" data-v-1939e84f>"+_vm._ssrEscape("\n                "+_vm._s(item.titulo_lista_responsabilidades)+"\n              ")+"</div> <div class=\"text-secondary text-content\" data-v-1939e84f>"+_vm._ssrEscape("\n                "+_vm._s(item.descricao_lista_responsabilidades)+"\n              ")+"</div></a> <a href=\"#\" class=\"nav-link w-fit border-bottom border-secondary fw-500 border-3 p-0 ps-0 text-secondary\" data-v-1939e84f>Saiba mais</a></li>")}))+"</ul></div> <div class=\"col-md-6\" data-v-1939e84f><div class=\"tab-content\" data-v-1939e84f>"+(_vm._ssrList((_vm.content.lista_responsabilidades),function(item,index){return ("<div"+(_vm._ssrAttr("id",("item-" + (item.id))))+" role=\"tabpanel\""+(_vm._ssrAttr("aria-labelledby",("tab-" + (item.id))))+(_vm._ssrClass("tab-pane",{ active: index === 0 }))+" data-v-1939e84f><div class=\"w-100 min-h-700\" data-v-1939e84f><img"+(_vm._ssrAttr("src",("" + (_vm.getImage(
                  item.imagem_lista_responsabilidades,
                  'md'
                )))))+" class=\"w-100 rounded-6\" data-v-1939e84f></div></div>")}))+"</div></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/responsabilidades.vue?vue&type=template&id=1939e84f&scoped=true&

// EXTERNAL MODULE: ./mixins/contentMixin.js
var contentMixin = __webpack_require__(60);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/responsabilidades.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var responsabilidadesvue_type_script_lang_js_ = ({
  name: 'Responsabilidades',
  mixins: [contentMixin["a" /* default */]],

  data() {
    return {};
  },

  computed: {
    content() {
      const content = this.getContent('responsabilidades');
      return content;
    }

  },

  async mounted() {
    // console.log(this.content, '<--')
    const bootstrap = __webpack_require__(59);

    await this.$nextTick();
    const triggerTabList = [].slice.call(document.querySelectorAll('#tab-items a.tab'));
    triggerTabList.forEach(function (triggerEl) {
      const tabTrigger = new bootstrap.Tab(triggerEl);
      triggerEl.addEventListener('click', function (event) {
        event.preventDefault();
        tabTrigger.show();
      });
    });
  }

});
// CONCATENATED MODULE: ./components/responsabilidades.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_responsabilidadesvue_type_script_lang_js_ = (responsabilidadesvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/responsabilidades.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(79)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_responsabilidadesvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "1939e84f",
  "59f6506c"
  
)

/* harmony default export */ var responsabilidades = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=responsabilidades.js.map