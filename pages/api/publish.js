import Cors from 'cors'

const cors = Cors({
  methods: ['GET', 'HEAD', 'POST'],
})

function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result)
      }

      return resolve(result)
    })
  })
}

async function handler(req, res) {

  const response = {}

  const json  = (req.query.json      != undefined)
  const url   = (req.query.permanent != undefined) ? `/${ req.query.permanent }` : '/'

  try {

    await runMiddleware(req, res, cors)
    await res.unstable_revalidate(url)

    response['response']  = true
  }
  catch (err) {

    // console.log(err)

    response['response']  = false
  }

  return (json) ? res.json(response) : res.redirect(307, `${ url }?cache=true`)
}

export default handler
