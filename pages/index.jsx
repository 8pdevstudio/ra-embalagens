import Api    from '/src/api'

import lodash from 'lodash'
import Template from '/src/default/template'
import Head from 'next/head'
import Navbar from '/src/sections/navbar'
import Vitrine from '/src/sections/vitrine'
import SobreNos from '/src/sections/sobre-nos'
import Numeros from '/src/sections/numeros'
import Produtos from '/src/sections/produtos'
import Cta1 from '/src/sections/cta1'
import Parcerias from '/src/sections/parcerias'
import Responsabilidades from '/src/sections/responsabilidades'
import Cta2 from '/src/sections/cta2'
import TrabalheConosco from '/src/sections/trabalhe-conosco'
import Blog from '/src/sections/blog'
import Newsletter from '/src/sections/newsletter'
import Depoimentos from '/src/sections/depoimentos'
import Contato from '/src/sections/contato'
import Rodape from '/src/sections/rodape'

export const getStaticProps = async () => {

  let graph_request = '{"vitrine":{},"sobre_nos":{},"numeros":{},"produtos":{},"cta1":{},"parcerias":{},"responsabilidades":{},"cta2":{},"trabalhe_conosco":{},"blog":{},"detalhes_blog":{},"newsletter":{},"detalhes_newsletter":{},"depoimentos":{},"contato":{},"lgpd_scripts":{},"configuracao":{}}'

  const api     = new Api()
  const content = await api.call('post', '', {graph_request:  graph_request}).then(({ data }) => data)

  return {
    props: {
      content: content.data
    },
    revalidate: 1
  }
}

export default function Home({ content }) {

  // console.log(content.vitrine.titulo_vitrine)

  return (
    <Template current="home" content={ content }>
      <Head>
        <title>{ content.vitrine.titulo_vitrine }</title>
      </Head>
      <section>
        <Vitrine content={ content } />
      </section>
      <section>
        <SobreNos content={ content } />
      </section>
      <section>
        <Numeros content={ content } />
      </section>
      <section>
        <Produtos content={ content } />
      </section>
      <section>
        <Cta1 content={ content } />
      </section>
      <section>
        <Parcerias content={ content } />
      </section>
      <section>
        <Responsabilidades content={ content } />
      </section>
      <section>
        <Cta2 content={ content } />
      </section>
      <section>
        <TrabalheConosco content={ content } />
      </section>
      <section>
        <Blog content={ content } />
      </section>
      <section>
        <Newsletter content={ content } />
      </section>
      <section>
        <Depoimentos content={ content } />
      </section>
      <section>
        <Contato content={ content } />
      </section>
    </Template>
  )
}
