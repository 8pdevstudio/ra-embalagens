import Api      from '/src/api'
import Request  from '/src/request'

import Template from '/src/default/template'
import Head     from 'next/head'
import Image    from 'next/image'
import Link     from 'next/link'

import ListBlog   from '/src/templates/list-blog'
import Newsletter from '/src/sections/newsletter'

const request = new Request()

export const getStaticProps = async () => {

  const api     = new Api()
  const content = await api.call('post', '', { graph_request: request.BlogProps() }).then(({ data }) => data)

  return {
    props: {
      content: content.data
    },
    revalidate: (1)
  }
}

export default function Blog({ content }) {

  return (
    <Template current="blog" content={ content }>

      <Head>
        <title>Blog - RA Embalagens</title>
      </Head>

      <ListBlog content={content} />

      <section>
        <Newsletter content={ content } />
      </section>

    </Template>
  )
}
