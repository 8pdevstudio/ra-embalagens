import React, { useState, useEffect } from "react";

import { useRouter } from 'next/router'

import Api      from '/src/api'
import Request  from '/src/request'

import Helper   from '/src/helper'
import Content  from '/src/content'

import Link         from 'next/link'
import Template     from '/src/default/template'
import Head         from 'next/head'
import Image        from 'next/image'
import Newsletter   from '/src/sections/newsletter'
// import PostBlog     from '/src/components/post-blog'

const request = new Request()
const helper  = new Helper()
const cont    = new Content()

export const getStaticPaths = async ( ) => {

  const api     = new Api()
  const content = await api.call('post', '', { graph_request: request.PostPaths() }).then(({ data }) => data)

  const paths = content.data.blog.map(post => {
    return { params: { slug: `${ post.slug }` }}
  })

  return {
    paths: paths,
    fallback: true,
  }
}

export const getStaticProps = async ( context ) => {

  const { slug } = context.params

  const api     = new Api()
  const content = await api.call('post', '', { graph_request: request.PostProps(slug) }).then(({ data }) => data)

  return {
    props: {
      content: content.data
    },
    revalidate: (1)
  }
}

export default function Post({ content }) {

  const { isFallback } = useRouter()

  if( isFallback ){
    return ''
  }

  let [base_url, setBase] = useState()

  let post          = content.blog[0]
  let related       = content.related
  let conteudo_blog = post.conteudo_blog ? post.conteudo_blog : {}

  let categoria   = post.categorias_blog ? post.categorias_blog : []
      categoria   = categoria.length ? categoria[0] : {}

  useEffect(() => {
    setBase(`${ window.location.origin }/blog/${ post.slug }`)
  })

  const NewsletterSection = () => {
    return (
      <>
        <Newsletter content={ content } />
      </>
    )
  }

  return (
    <Template header="fixed" current="blog" content={ content }>

      <Head>
        <title>Post - RA Embalagens</title>
      </Head>

      <section className="position-relative">
        <div className="container-fluid position-relative">

          <div className="pt-48">
            <div className="container">
              <div className="max-w-1000 mx-auto position-relative z-index-1">
                <div className="mb-24 text-danger fw-700 d-flex align-items-center">
                  <div className="fs-5">
                    Início
                  </div>
                  <div>
                    <i className="fa fa-chevron-right mt-2 mx-8 small"></i>
                  </div>
                  <div className="fs-5">
                    <Link href={ `/blog` }>
                      <a className="text-danger">
                        Blog
                      </a>
                    </Link>
                  </div>
                </div>
                <h1 className="font-title font-32 font-md-56 fw-700">
                  { post.titulo_blog }
                </h1>
                <div className="d-flex align-items-center">
                  <div className="py-16">
                    { helper.getExtenseData( post.created_at ) }
                  </div>
                  <div className="px-8">|</div>

                  <Link href={ `/blog/categoria/${ categoria.slug ?? '' }` }>
                    <a className="text-dark">
                      <div className="">
                        { categoria.titulo_categorias_blog ?? '' }
                      </div>
                    </a>
                  </Link>

                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section className="position-relative pt-88 pt-md-96">
        <div className="container">
          
          <div className="max-w-1000 mx-auto">

            <div className="row justify-content-between">

              <div className="col-auto pt-1 mt--48">
                <div className="text-center sticky-top pt-48">
                  <div className="pb-32">
                    
                    <a href={`https://www.facebook.com/sharer.php?u=${ base_url }`} target="_blank" className="text-danger">
                      <i className="fab fa-facebook-f fs-5"></i>
                    </a>

                  </div>
                  <div className="pb-32">

                    <a href={`https://twitter.com/share?url=${ base_url }`} target="_blank" className="text-danger">
                      <i className="fab fa-twitter fs-5"></i>
                    </a>

                  </div>
                  <div className="pb-32">

                    <a href={`https://www.linkedin.com/shareArticle?mini=true&amp;url=${ base_url }`} target="_blank" className="text-danger">
                      <i className="fab fa-linkedin-in fs-5"></i>
                    </a>
                    
                  </div>
                  <div className="pb-32">

                    <a href={`https://api.whatsapp.com/send?text=${ base_url }`} target="_blank" className="text-danger">
                      <i className="fab fa-whatsapp fs-5"></i>
                    </a>
                    
                  </div>
                </div>
              </div>

              <div className="col-12 max-w-750">
                
                <div className="font-md-18 text-content">
                  {
                    Object.values(conteudo_blog).map((block, key) => {

                      if(block.type == 'Image'){

                        let image = JSON.parse(block.content.preview[0].details)

                        let path = image.sizes[2] ? image.sizes[2] : image.sizes.slice(-1)[0]
                            path = path.path

                        let src   = cont.fixAbsolutePathImage(image.absolute_path) + path

                        return (
                          <div
                            key={key}
                            className="pb-32"
                          >
                            <img loading="lazy" src={ src } className="img-fluid"/>
                          </div>
                        )                      
                      }

                      if(block.type == 'Gallery'){

                        let gallery = ''
                        let absolute_path = cont.fixAbsolutePathImage(block.content.absolute_path)

                        block.content.preview.map((image, key) => {

                          let img = JSON.parse(image.details)

                          let path = img.sizes[2] ? img.sizes[2] : img.sizes.slice(-1)[0]
                              path = path.path

                          let address = absolute_path + path

                          gallery += `
                                      <div class="pb-24">
                                        <img loading="lazy" src="${ cont.fixAbsolutePathImage(address) }" class="img-fluid" />
                                      </div>
                                    `
                        })

                        return (
                          <div
                            key={key}
                            dangerouslySetInnerHTML={{__html: gallery }}
                          >                        
                          </div>
                        )
                      }

                      if(block.type == 'Video'){
                        return (
                          <div
                            key={key}
                            className="pb-48 pb-md-64"
                          >
                            <div className="ratio ratio-16x9">
                              <iframe className={`bg-image`} id={ key } loading="lazy" width="100%" height="100%" src={`https://www.youtube.com/embed/${ helper.YoutubeId(block.content) }?rel=0&showinfo=0&modestbranding=1&theme=dark`} frameBorder="0" allow="accelerometer; autoplay; modestbranding; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            </div>
                          </div>
                        )
                      }

                      return (
                        <div
                          key={key}
                          className="pb-24"
                          dangerouslySetInnerHTML={{ __html: block.content }}
                        >
                        </div>
                      )

                    })
                  }
                </div>

                <div className="d-none">
                  <div className="py-24 text-danger small fw-bold">

                    <a className="d-inline-block pr-8 text-danger">
                      Shows
                    </a>                                    
                  
                    <a className="d-inline-block pr-8 text-danger">
                      Eventos corporativos
                    </a>                                    
                  
                    <a className="d-inline-block pr-8 text-danger">
                      Publicidade
                    </a>                                    
                  
                    <a className="d-inline-block pr-8 text-danger">
                      Videoclipes
                    </a>                                    
                  
                    <a className="d-inline-block pr-8 text-danger">
                      Projetos de Imersão
                    </a>

                  </div>
                </div>
              </div>

              <div className="col-auto"></div>

            </div>

          </div>          

          <div className="py-md-32 mt-64 mt-md-96">
            
            <div className="d-none max-w-700 mx-auto position-relative z-index-1">
              <h6 className="font-title text-center font-32 font-md-40 fw-700 mb-md-32 mb-lg-56">
                Veja também
              </h6>
            </div>
            
            <div className="d-none row justify-content-between">

              <div className="col-12 col-md-4 opacity-in mb-72 mb-md-96">
                <div className="ratio ratio-16x9 bg-light rounded-6 overflow-hidden">
                  <img loading="lazy" className="bg-image" src="https://images.unsplash.com/photo-1614177388746-2c903bcf572f?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=500" />
                </div>
                <div className="">
                  <div className="text-muted pb-8 pt-16">
                    18 de outubro de 2022
                  </div>                  
                  <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                    <a className="text-dark">
                      <h5 className="font-24 font-title fw-700">
                        Complicated by his illegitimate daughter’s discovery of the creature. 
                      </h5>
                    </a>
                  </Link>
                  <div className="pr-md-24 opacity-in-target mt-16">
                    <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                      <a className="fw-500 text-danger border-bottom border-2 border-danger">
                        Acessar
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
              
              <div className="col-12 col-md-4 opacity-in mb-72 mb-md-96">
                <div className="ratio ratio-16x9 bg-light rounded-6 overflow-hidden">
                  <img loading="lazy" className="bg-image" src="https://images.unsplash.com/photo-1597974828431-9078248d8cc9?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=500" />
                </div>
                <div className="">
                  <div className="text-muted pb-8 pt-16">
                    18 de outubro de 2022
                  </div>                  
                  <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                    <a className="text-dark">
                      <h5 className="font-24 font-title fw-700">
                        Complicated by his illegitimate daughter’s discovery of the creature. 
                      </h5>
                    </a>
                  </Link>
                  <div className="pr-md-24 opacity-in-target mt-16">
                    <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                      <a className="fw-500 text-danger border-bottom border-2 border-danger">
                        Acessar
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
              
              <div className="col-12 col-md-4 opacity-in mb-72 mb-md-96">
                <div className="ratio ratio-16x9 bg-light rounded-6 overflow-hidden">
                  <img loading="lazy" className="bg-image" src="https://images.unsplash.com/photo-1595246135406-803418233494?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=500" />
                </div>
                <div className="">
                  <div className="text-muted pb-8 pt-16">
                    18 de outubro de 2022
                  </div>                  
                  <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                    <a className="text-dark">
                      <h5 className="font-24 font-title fw-700">
                        Complicated by his illegitimate daughter’s discovery of the creature. 
                      </h5>
                    </a>
                  </Link>
                  <div className="pr-md-24 opacity-in-target mt-16">
                    <Link href={{ pathname: '/blog', query: { slug: 'lorem' } }}>
                      <a className="fw-500 text-danger border-bottom border-2 border-danger">
                        Acessar
                      </a>
                    </Link>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
      </section>

      { NewsletterSection() }

    </Template>
  )
}
