import Api      from '/src/api'
import Request  from '/src/request'

import { useRouter } from 'next/router'

import Template from '/src/default/template'
import Head     from 'next/head'
import Image    from 'next/image'
import Link     from 'next/link'

import ListBlog   from '/src/templates/list-blog'
import Newsletter from '/src/sections/newsletter'

const request = new Request()

export const getStaticPaths = async ( ) => {

  const api     = new Api()
  const content = await api.call('post', '', { graph_request: request.BlogCategoriaPaths() }).then(({ data }) => data)

  const paths = content.data.categorias_blog.map(post => {
    return { params: { slug: `${ post.slug }` }}
  })

  return {
    paths: paths,
    fallback: true,
  }
}

export const getStaticProps = async ( context ) => {

  const { slug } = context.params

  const api     = new Api()
  const content = await api.call('post', '', { graph_request: request.BlogCategoriaProps(slug) }).then(({ data }) => data)

  let categoria = content.data.categorias_blog.filter((item, key) => {
    return (item.slug == slug)
  })[0]

  return {
    props: {
      content: content.data,
      current: categoria ? categoria : {}
    },
    revalidate: (1)
  }
}

export default function Blog({ content, current = false }) {

  const { isFallback } = useRouter()

  if( isFallback ){
    return ''
  }

  return (
    <Template current="blog" content={ content }>

      <Head>
        <title>Blog - RA Embalagens</title>
      </Head>

      <ListBlog content={content} current={current} />

      <section>
        <Newsletter content={ content } />
      </section>

    </Template>
  )
}
