export default function Cta2({ content }) {
  const cta2 = content.cta2

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  return (
    <div className="container-fluid position-relative mt--144 bg-danger">
      <img
        src={ getImage(cta2.imagem_fundo_cta2, 'lg')}
        className="bg-image"
      />
      <div className={ `${ getImage(cta2.imagem_fundo_cta2, 'sm') ? '' : 'd-none' } position-absolute top-0 start-0 w-100 h-100 bg-dark bg-opacity-75` }></div>
      <div className="container pt-144">
        <div className="z-index-1 w-100 py-88 py-lg-144 d-flex justify-content-center">
          <div className="position-relative px-lg-128">
            <h2 className="font-title font-40 font-md-72 text-white fw-700">
              {cta2.chamada_cta2}
            </h2>
            <a href={cta2.link_cta2} className="text-dark">
              <div className="bg-white rounded-7 p-16 w-fit mt-32">
                <div className="row gx-3 align-items-center">
                  <div className="col-auto">
                    <div className={ `${ getImage(cta2.miniatura_botao_cta2,'thumb') ? '' : 'd-none' } p-32 p-md-40 rounded-6 text-dark position-relative overflow-hidden` }>
                      <img
                        src={ getImage(
                          cta2.miniatura_botao_cta2,
                          'thumb'
                        )}
                        className="bg-image"
                      />
                    </div>
                  </div>
                  <div className="col pr-md-16">
                    <div className="lh-h">
                      <div className="mb-0 font-md-24 fw-600">
                        {cta2.titulo_botao_cta2}
                      </div>
                      <div className="text-muted">
                        {cta2.descricao_botao_cta2}
                      </div>
                    </div>
                  </div>
                  <div className="col-auto">
                    <div className="bg-danger p-32 p-md-40 rounded-6 text-white position-relative overflow-hidden">
                      <i
                        className={`${cta2.icone_botao_cta2} font-32 position-absolute top-50 start-50 translate-middle`}
                      ></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>

            <div className="cta2-ear position-absolute top-0 end-0 d-none d-md-block"></div>
          </div>
        </div>
      </div>
    </div>
  )
}
