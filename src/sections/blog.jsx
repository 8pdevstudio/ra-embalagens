import Link from 'next/link'

import { Swiper, SwiperSlide }    from 'swiper/react'
import { Pagination, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

export default function Blog({ content }) {

  const blog  = content.detalhes_blog
  const posts = content.blog

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  return (
    <div>
      <div id="blog" className="position-relative overflow-hidden py-64 pt-xl-144 my-xl-64">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="row align-items-end">
                <div className="col-xl">
                  <div className="position-relative">
                    <div className="blog-ear d-none d-lg-block"></div>
                    <p className="text-danger font-md-18 fw-700 mb-24">
                      {blog.titulo_detalhes_blog}
                    </p>
                    <h2 className="font-title font-32 font-md-56 fw-700 mb-32 mb-xl-0">
                      {blog.chamada_detalhes_blog}
                    </h2>
                  </div>
                </div>
                <div className="col-xl-auto">
                  {blog.redirecionamento_detalhes_blog ? (
                    <div className="btn-group">
                      <a
                        href={blog.redirecionamento_detalhes_blog.text}
                        target={blog.redirecionamento_detalhes_blog.tab}
                        className="btn btn-danger btn-lg rounded-5 pl-40 pr-96 py-16 position-relative"
                      >
                        {blog.redirecionamento_detalhes_blog.label
                          ? blog.redirecionamento_detalhes_blog.label
                          : 'Ver todos'}
                        <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-24 d-flex align-items-center h-100">
                          <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                        </div>
                      </a>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="position-relative w-100 mt-32 mt-xl-112">
          <Swiper
            className="container swiper swiper-visible py-56 py-md-96"
            pagination={true}
            modules={[Pagination, Navigation]}
            centeredSlides={true}
            spaceBetween={144}
            slidesPerView={1}
          >
            {
              posts.map((post, key) => {

                // console.log( getImage(post.imagem_blog, 'md') , '<')

                return (
                  <SwiperSlide className="swiper-slide" key={key}>
                    <div className="container pb-48 py-md-56 pl-lg-64">
                      
                      <div className="position-absolute h-100 w-100 w-xl-90 bg-theme-light top-0 start-0 d-none d-xl-block"></div>

                      <div className="row align-items-center position-relative h-100">
                        <div className="col-xl-6 pr-xl-64 mb-32 mb-xl-0">
                          <Link href={{ pathname: '/blog', query: { slug: post.slug } }}>
                            <a className="text-dark">
                              <h2 className="font-title font-32 font-md-40 fw-700">
                                {post.titulo_blog}
                              </h2>
                            </a>
                          </Link>
                          <div className="text-secondary py-24 font-md-18">
                            {post.descricao_breve_blog}
                          </div>
                          <Link href={{ pathname: '/blog', query: { slug: post.slug } }}>
                            <a className="fw-500 text-danger border-bottom border-2 border-danger">
                              Leia mais
                            </a>
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          <Link href={{ pathname: '/blog', query: { slug: post.slug } }}>
                            <a>
                              <div className="ratio ratio-4x3 position-relative">
                                <img
                                  loading="lazy"
                                  src={ getImage(post.imagem_blog, 'md') }
                                  className="swiper-lazy w-100 h-100 image-cover rounded-6"
                                />
                              </div>
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )

              })
            }
          </Swiper>
        </div>
      </div>
    </div>
  )
}
