import { Swiper, SwiperSlide } from 'swiper/react'
import { Pagination, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

import Helper from '/src/helper.js'

const helper = new Helper()

export default function Produtos({ content }) {
  const produtos = content.produtos

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  const sublistaProdutos = (sublistaProdutos) => {
    let sublista = []

    if (!sublistaProdutos.length) {
      return sublista
    }

    for (const [index, produto] of sublistaProdutos.entries()) {
      sublista.push(
        <div className="col-12 col-lg-6 col-xl-3 pt-32" key={index}>
          <div className="rounded-6 bg-white position-relative h-100 overflow-hidden">
            <div className="hover-effect">
              <div className="ratio ratio-4x3 position-relative">
                <img
                  className="bg-image"
                  src={getImage(produto.imagem_sublista_produtos, 'sm')}
                />
              </div>

              <div className="p-16 p-md-4">
                <div className="d-flex align-items-center">
                  <div className="rounded-circle d-inline-block bg-danger w-auto text-white p-16 position-relative">
                    <div className="p-1"></div>
                    <i
                      className={`${produto.icone_sublista_produtos} font-14 position-absolute top-50 start-50 translate-middle`}
                    ></i>
                  </div>
                  <div className="fw-800 font-24 font-xl-16 font-xxl-24 pl-16 lh-h w-70">
                    {produto.titulo_sublista_produtos}
                  </div>
                </div>
              </div>

              <div className="position-absolute top-0 start-0 w-100 h-100 bg-danger rounded-3 hover-effect-target p-16">
                <div className="position-relative m-8 h-100 pb-56">
                  <div className="d-flex align-items-center">
                    <div className="rounded-circle d-inline-block bg-white w-auto text-white p-16 position-relative">
                      <div className="p-1"></div>
                      <i
                        className={`${produto.icone_sublista_produtos} text-danger font-16 position-absolute top-50 start-50 translate-middle`}
                      ></i>
                    </div>

                    <div className="font-title fw-600 pl-16 text-white font-24 font-xl-16 font-xxl-24 lh-h">
                      {produto.titulo_sublista_produtos}
                    </div>
                  </div>

                  <div className="lh-body pt-24 font-14 font-xxl-16 text-white">
                    {produto.descricao_sublista_produtos}
                  </div>

                  <div className="position-absolute w-100 bottom-0 pb-16 start-0">
                    <a href="#" className="text-light fw-bold">
                      Baixe o catálogo completo <i className="far ms-2 small fa-chevron-down"></i>
                    </a>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }

    return sublista
  }

  const fotos = (galeria) => {
    let fotos = []

    if (!galeria.length) {
      return fotos
    }

    for (const [index, gal] of galeria.entries()) {
      fotos.push(
        <SwiperSlide className="swiper-slide" key={index}>
          <div className="ratio ratio-4x3 rounded-6 overflow-hidden">
            <img
              loading="lazy"
              src={gal.md}
              className="w-100 h-100 bg-image image-contain"
            />
          </div>
        </SwiperSlide>
      )
    }

    return fotos
  }

  return (
    <div className="px-md-64">
      <div
        id="produtos"
        className="container-fluid bg-theme-light position-relative z-index-1"
      >
        <div className="container pt-72 pt-md-144">
          <div className="row text-center mb-24">
            <div className="col">
              <h2 className="font-title fw-700 font-32 font-md-56">
                {produtos.titulo_produtos}
              </h2>
            </div>
            <div className="col-12 mt-16">
              <div className="text-secondary mx-auto max-w-950 font-md-18 pb-8">
                {produtos.descricao_produtos}
              </div>
            </div>
          </div>
          <div className="row">{sublistaProdutos(produtos.sublista_produtos)}</div>
        </div>

        <div className="row pb-88 pb-md-132 pt-32 pt-md-112">
          <div className="position-relative w-100 overflow-hidden">

            <div className="d-none d-md-block position-absolute z-index-2 w-100 top-50 start-0 translate-middle-y">
              <div className="container">
                <div className="d-flex align-items-center justify-content-between h-0">
                  <button className="slide-fotos-prev btn btn-white text-danger p-3 lh-1 mr-24">
                    <i className="far fa-chevron-left mx-2 my-1 font-md-18"></i>
                  </button>
                  <button className="slide-fotos-next btn btn-white text-danger p-3 lh-1">
                    <i className="far fa-chevron-right mx-2 my-1 font-md-18"></i>
                  </button>                  
                </div>
              </div>
            </div>

            <div className="mx-xl--32 px-0">
              <Swiper
                centeredSlides={true}
                className="swiper swiper-visible pb-56"
                pagination={{
                  clickable: true,
                }}
                navigation={{
                  nextEl: '.slide-fotos-next',
                  prevEl: '.slide-fotos-prev',
                }}
                modules={[Pagination, Navigation]}
                spaceBetween={32}
                slidesPerView={1}
                breakpoints={{
                  640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                  },
                  768: {
                    slidesPerView: 3,
                    spaceBetween: 40,
                  }
                }}
              >
                {fotos(produtos.galeria_produtos)}
              </Swiper>
            </div>

          </div>

        </div>

      </div>
    </div>
  )
}
