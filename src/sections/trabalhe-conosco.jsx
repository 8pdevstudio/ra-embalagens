export default function TrabalheConosco({ content }) {
  const trabalhe = content.trabalhe_conosco

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  const blog = (posts = []) => {
    let all = []

    if (!posts.length) {
      return all
    }

    for (const [index, post] of posts.entries()) {
      all.push(
        <div key={index} className="bg-white max-w-600 p-32 p-md-40">
          <div className="font-title fw-700 font-32 font-md-40 lh-h">
            {post.titulo_blog}
          </div>
          <div className="text-secondary font-16 py-16 font-md-18">
            {post.descricao_breve_blog}
          </div>
          <a href={{ pathname: '/blog', query: { slug: post.slug } }} className="fw-500 text-danger font-md-18">
            Leia mais
          </a>
          {post.imagem_blog.length > 0 ? (
            <div className="ratio ratio-16x9 rounded-6 bg-theme-light mt-24 mt-md-32 overflow-hidden">
              <img src={getImage(post.imagem_blog, 'sm')} />
            </div>
          ) : (
            ''
          )}
        </div>
      )
    }

    return all
  }
  return (
    <div className="bg-theme-light py-64 py-xl-144">
      <div className="container">
        <div className="">
          <div className="row justify-content-center">
            <div className={`${ trabalhe.post_trabalhe_conosco.blog ? '' : 'text-center' } col-xl-6`}>
              <div className="max-w-900">
                <div className="mb-md-16">
                  <div className="text-danger font-md-18 fw-700 mb-24">
                    {trabalhe.titulo_trabalhe_conosco}
                  </div>
                  <h2 className="font-title font-32 font-md-56 mb-24 mb-md-0 fw-700">
                    {trabalhe.chamada_trabalhe_conosco}
                  </h2>
                </div>
                <div className="text-secondary font-md-24 mb-24 mb-md-48">
                  {trabalhe.descricao_trabalhe_conosco}
                </div>
                <div className="btn-group">
                  {trabalhe.redirecionamento_trabalhe_conosco ? (
                    <button
                      // href={trabalhe.redirecionamento_trabalhe_conosco.text}
                      className="btn btn-danger btn-lg rounded-5 pl-40 pr-112 py-24 position-relative"
                      data-bs-toggle="modal" data-bs-target="#exampleModal"
                    >
                      {trabalhe.redirecionamento_trabalhe_conosco.label}
                      <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-40 d-flex align-items-center h-100">
                        <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                      </div>
                    </button>
                  ) : (
                    ''
                  )}
                </div>
              </div>
            </div>
            {trabalhe.post_trabalhe_conosco.blog ? (
              <div className="col-md-6 d-flex pt-md-144 justify-content-end">
                {blog(trabalhe.post_trabalhe_conosco.blog)}
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
      </div>

      <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content rounded-6 p-md-16">
            <div className="modal-header border-0">
              <h5 className="modal-title" id="exampleModalLabel">
                {trabalhe.redirecionamento_trabalhe_conosco.label}
              </h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-12">
                  <div className="mb-4">
                    <label className="font-14 mb-1">Nome</label>
                    <input
                      type="text"
                      className="form-control rounded-0 p-8"
                    />
                  </div>
                </div>
                <div className="col-6">
                  <div className="mb-4">
                    <label className="font-14 mb-1">E-mail</label>
                    <input
                      type="email"
                      className="form-control rounded-0 p-8"
                    />
                  </div>
                </div>
                <div className="col-6">
                  <div className="mb-4">
                    <label className="font-14 mb-1">Telefone ou WhatsApp</label>
                    <input
                      type="tel"
                      className="form-control rounded-0 p-8"
                    />
                  </div>
                </div>
                <div className="col-12">
                  <div className="mb-4">
                    <label className="font-14 mb-1">
                      Sua mensagem
                    </label>
                    <textarea className="form-control rounded-0 p-8"></textarea>
                  </div>
                </div>
                <div className="col-12">
                  <div className="mb-4">
                    <label className="font-14 mb-1">Currículo <small>(.pdf)</small></label>
                    <input
                      type="file"
                      className="form-control rounded-0"
                    />
                  </div>
                </div>
              </div>
              <div className="btn-group mt-16 w-100">
                <button type="button" className="btn btn-danger d-flex justify-content-between pl-24 rounded-5 py-16 position-relative">
                  Confirmar e enviar
                  <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-32 d-flex align-items-center h-100">
                    <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                  </div>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
