export default function Contato({ content }) {
  const contato = content.contato

  return (
    <div id="contato" className="position-relative overflow-hidden">
      <div className="px-md-64 py-md-72">
        <div className="container-fluid bg-theme-light pt-64 pb-24 py-xl-144">
          <div className="container">
            <div className="row justify-content-between">
              <div className="col-xl-5 mt--48 mt-md--64">
                <div className="sticky-top pt-32 pt-xl-64">
                  <p className="text-danger font-md-18 fw-700 mb-16">
                    {contato.titulo_contato}
                  </p>
                  <h3 className="font-title font-32 font-xl-56 fw-700 mb-24">
                    {contato.chamada_contato}
                  </h3>
                  <div className="mb-32 max-w-400">
                    {/*<p className="text-secondary mb-1 pt-3">Endereço</p>
                    <a
                      href={`https://www.google.com.br/maps/search/${contato.endereco_contato}`}
                      target="_blank"
                    >
                      <p className="text-danger mb-16 fw-700">
                        {contato.endereco_contato}
                      </p>
                    </a>*/}
                    <p className="text-secondary mb-1 pt-3">Ligue</p>
                    <a
                      href={`tel:55${contato.telefone_contato.replace(
                        /\D/g,
                        ''
                      )}`}
                    >
                      <p className="text-danger fw-700 font-14 font-md-18 mb-0">
                        {contato.telefone_contato}
                      </p>
                    </a>
                    {/*<p className="text-secondary opacity-50 font-16 mb-16">
                      {contato.horario_contato}
                    </p>*/}
                    <div className="py-md-8"></div>
                    <p className="text-secondary mb-1 pt-3">Mande um e-mail</p>
                    <a href={`mailto:${contato.email_contato}`}>
                      <p className="text-danger fw-700 font-14 font-md-18">
                        {contato.email_contato}
                      </p>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-xl-6">
                <div className="row">
                  <div className="col-12">
                    <div className="mb-4">
                      <label className="font-md-18 mb-md-8">Nome</label>
                      <input
                        type="text"
                        className="form-control form-control-lg border-danger rounded-0 p-16"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-md-6">
                    <div className="mb-4">
                      <label className="font-md-18 mb-md-8">E-mail</label>
                      <input
                        type="email"
                        className="form-control form-control-lg border-danger rounded-0 p-16"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-md-6">
                    <div className="mb-4">
                      <label className="font-md-18 mb-md-8">Telefone ou WhatsApp</label>
                      <input
                        type="tel"
                        className="form-control form-control-lg border-danger rounded-0 p-16"
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="mb-4">
                      <label className="font-md-18 mb-md-8">Assunto</label>
                      <input
                        type="text"
                        className="form-control form-control-lg border-danger rounded-0 p-16"
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="mb-4">
                      <label className="font-md-18 mb-md-8">
                        Sua mensagem (opcional)
                      </label>
                      <textarea className="form-control form-control-lg border-danger rounded-0 p-16"></textarea>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="mb-16">
                      <label className="d-flex align-items-start">
                        <input required type="checkbox" className="mt-1 me-1" />
                        <div
                          dangerouslySetInnerHTML={{
                            __html: contato.texto_consentimento_contato,
                          }}
                          className="form-check-label small pl-8 max-w-500"
                        ></div>
                      </label>
                    </div>
                  </div>
                  <div className="col-12 pt-8">
                    <div className="btn-group w-100">
                      <a className="btn btn-danger btn-lg rounded-5 pl-40 pr-112 py-24 position-relative">
                        {contato.botao_contato}
                        <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-40 d-flex align-items-center h-100">
                          <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-secondary position-relative">
        <div
          dangerouslySetInnerHTML={{
            __html: contato.codigo_mapa_contato,
          }}
          className="ratio maps"
        ></div>
      </div>
    </div>
  )
}
