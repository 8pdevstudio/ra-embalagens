import Link from 'next/link'

import { Swiper, SwiperSlide } from 'swiper/react'
import { Pagination, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

export default function Vitrine({ content }) {
  const vitrine = content.vitrine

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  const slidesVitrine = (slidesVitrine) => {
    let slides = []

    if (!slidesVitrine.length) {
      return slides
    }

    for (const [index, slide] of slidesVitrine.entries()) {
      slides.push(
        <SwiperSlide className="swiper-slide" key={index}>
          <div className="container-fluid px-xl-64">
            <div className="slider-content position-relative w-100 min-h-550 max-h-750 rounded-7 overflow-hidden d-flex align-items-center bg-light">
              
              <div className="position-absolute top-50 end-0 h-100 d-flex justify-content-center translate-middle-y w-100">
                <img
                  src={getImage(slide.imagem_fundo_slides_vitrine, 'xl')}
                  className="bg-image"
                />
              </div>

              {/*<div className="bg-image bg-dark opacity-75"></div>*/}

              <div className={`${ slide.tipo_slides_vitrine == 'image' ? 'opacity-0' : '' } p-32 p-md-64 p-xl-144 position-relative z-index-1`}>
                <div className="max-w-500 max-w-xl-700 py-lg-64">

                  <div className={`${ slide.tipo_slides_vitrine == 'overlay' ? 'text-white text-shadow' : '' }`}>
                    <h2 className="font-title font-32 font-md-40 font-xl-64 fw-700 mb-16 mb-24 lh-h">
                      {slide.title}
                    </h2>
                    <h2 className="font-16 font-md-24 fw-500 py-md-8 mb-md-32 max-w-500">
                      {slide.descricao_slides_vitrine}
                    </h2>
                  </div>

                  {slide.redirecionamento_slides_vitrine.text ? (
                    <div>
                      <a
                        href={slide.redirecionamento_slides_vitrine.text}
                        target={slide.redirecionamento_slides_vitrine.tab[0]}
                        className="btn btn-danger border-0 btn-lg rounded-5 font-16 font-xl-18 pl-md-24 pl-xl-40 pr-72 pr-xl-112 py-16 py-xl-24 position-relative"
                      >
                        {slide.redirecionamento_slides_vitrine.label
                          ? slide.redirecionamento_slides_vitrine.label
                          : 'saiba mais'}
                        <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-24 px-xl-40 d-flex align-items-center h-100">
                          <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                        </div>
                      </a>
                    </div>
                  ) : (
                    ''
                  )}

                </div>
              </div>

            </div>
          </div>
        </SwiperSlide>
      )
    }
    return slides
  }

  return vitrine.slides_vitrine.length ? (
    <div className="position-relative pb-40 pb-md-88">
      <div className="position-absolute bottom-0 end-0 h-80 w-70 bg-theme-light"></div>
      <div className="position-relative z-index-1">
        <Swiper
          className="pb-56 "
          pagination={{
            clickable: true,
          }}
          modules={[Pagination, Navigation]}
          loop={true}
        >
          { slidesVitrine(vitrine.slides_vitrine )}
        </Swiper>
      </div>    
      <style jsx>
        {``}
      </style>
    </div>
  ) : (
    ''
  )
}