import React, { useEffect } from 'react'

export default function Responsabilidades({ content }) {
  const responsa = content.responsabilidades

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  const listaResponsabilidadesLi = (listaReponsabilidades) => {
    let lista = []

    if (!listaReponsabilidades.length) {
      return lista
    }

    useEffect(() => {
      if (typeof document !== undefined) {
        const bootstrap = require('bootstrap/dist/js/bootstrap')

        const triggerTabList = [].slice.call(
          document.querySelectorAll('#tab-items a.tab')
        )

        triggerTabList.forEach(function (triggerEl) {
          const tabTrigger = new bootstrap.Tab(triggerEl)

          triggerEl.addEventListener('click', function (event) {
            event.preventDefault()
            tabTrigger.show()
          })
        })
      }
    })

    for (const [index, resp] of listaReponsabilidades.entries()) {
      lista.push(
        <li
          className="nav-item w-100 mb-8"
          role="presentation"
          key={index}
        >
          <a
            href="#"
            className={`nav-link tab ps-0 text-secondary ${
              index === 0 ? 'active' : ''
            }`}
            id={`tab-${resp.id}`}
            data-bs-toggle="tab"
            data-bs-target={`#item-${resp.id}`}
            role="tab"
            aria-controls={`item-${resp.id}`}
            aria-selected="false"
          >
            <div className="font-title fw-700 mb-8 font-24">
              {resp.titulo_lista_responsabilidades}
            </div>
            <div className="active-target text-secondary max-w-450 font-16 text-content">
              {resp.descricao_lista_responsabilidades}
            </div>
            <div className="active-target mb-md-32">
              <div
                className="nav-link mt-24 w-fit border-bottom border-danger border-secondary fw-500 border-3 p-0 ps-0 text-danger"
              >
                Saiba mais
              </div>
            </div>
          </a>
        </li>
      )
    }

    return lista
  }

  const listaResponsabilidadesImg = (listaReponsabilidades) => {
    let lista = []

    if (!listaReponsabilidades.length) {
      return lista
    }

    for (const [index, resp] of listaReponsabilidades.entries()) {
      lista.push(
        <div
          className={`tab-pane ${index === 0 ? 'active' : ''}`}
          id={`item-${resp.id}`}
          role="tabpanel"
          aria-labelledby={`tab-${resp.id}`}
          key={index}
        >
          <img
            src={getImage(resp.imagem_lista_responsabilidades, 'md')}
            className="w-100 rounded-6 mt-32 mt-xl-0"
          />
        </div>
      )
    }

    return lista
  }

  return (
    <div className="px-md-64">
      <div
        id="responsabilidades"
        className="container-fluid bg-theme-light py-64 py-xl-144 position-relative z-index-1 d-flex align-items-center flex-column"
      >
        <div className="container">
          {/*
          <div className="text-danger font-md-18 fw-700 mb-24">
            {responsa.titulo_responsabilidades}
          </div>
          */}
          <h2 className="font-title font-32 font-md-56 fw-700 max-w-450 mb-md-32">
            {responsa.chamada_responsabilidades}
          </h2>
          <div className="text-secondary font-18 mb-64 max-w-550">
            {responsa.descricao_responsabilidades}
          </div>

          <div className="row align-items-center">
            <div className="col-xl-6">
              <ul className="nav ps-0 max-w-550" id="tab-items" role="tablist">
                {listaResponsabilidadesLi(responsa.lista_responsabilidades)}
              </ul>
            </div>
            <div className="col-xl-6">
              <div className="sticky-top">
                <div className="tab-content">
                  {listaResponsabilidadesImg(responsa.lista_responsabilidades)}
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  )
}
