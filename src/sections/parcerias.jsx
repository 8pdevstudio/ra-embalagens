export default function Parcerias({ content }) {
  const parcerias = content.parcerias

  const motivosParcerias = (motivos_parcerias) => {
    let motivos = []

    if (!motivos_parcerias.length) {
      return motivos
    }

    for (const [index, motivo] of motivos_parcerias.entries()) {
      motivos.push(
        <div className="col-12 col-xl-6 mt-48" key={index}>
          <div className="d-flex">
            <div className="pt-2">
              <div className="bg-danger text-white rounded-6 p-32 p-xl-48 position-relative">
                <i
                  className={`${motivo.icone_motivos_parcerias}  fs-3 position-absolute top-50 start-50 translate-middle`}
                ></i>
              </div>
            </div>
            <div className="w-100 pl-16 px-md-24 pt-1 pr-lg-72">
              <div className="font-title text-danger mb-8 font-24 fw-700">
                {motivo.titulo_motivos_parcerias}
              </div>
              <div className="font-16 font-md-18">{motivo.descricao_motivos_parcerias}</div>
            </div>
          </div>
        </div>
      )
    }
    return motivos
  }
  return (
    <div id="parcerias" className="py-64 py-md-144">
      <div className="container-xxl d-flex justify-content-center">
        <div className="container">
          <div className="max-w-900 mb-md-32">
            <div className="text-danger font-md-18 fw-700 mb-24">
              {parcerias.titulo_parcerias}
            </div>
            <h2 className="font-title font-32 font-md-64 fw-700">
              {parcerias.chamada_parcerias}
            </h2>
          </div>
          <div className="row">
            {motivosParcerias(parcerias.motivos_parcerias)}
          </div>
        </div>
      </div>
    </div>
  )
}
