export default function Cta1({ content }) {
  const cta1 = content.cta1

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  return (
    <div>
      <div className="container-fluid position-relative mt--144 bg-danger">
        <img
          src={ getImage(cta1.imagem_fundo_cta1, 'xl') }
          className="bg-image"
        />

        <div className={ `${ getImage(cta1.imagem_fundo_cta1, 'sm') ? '' : 'd-none' } position-absolute top-0 start-0 w-100 h-100 bg-dark bg-opacity-75` }></div>

        <div className="container position-relative pt-144">
          <div className="z-index-1 w-100 py-88 py-lg-144 d-flex justify-content-center">
            <div className="position-relative px-xxl-144">
              <h2 className="font-title font-40 font-md-64 text-white max-w-900 fw-700">
                {cta1.chamada_cta1}
              </h2>
              <div className="text-white py-24 max-w-600 font-md-18">
                {cta1.descricao_cta1}
              </div>
              <a href={`${cta1.link_cta1}`} className="text-dark">
                <div className="bg-white rounded-7 p-16 w-fit mt-32">
                  <div className="row gx-3 align-items-center">
                    <div className="col-auto">
                      <div className={`${ getImage(cta1.miniatura_botao_cta1, 'xl') ? '' : 'd-none' } p-32 p-md-40 rounded-6 text-dark position-relative overflow-hidden`}>
                        <img
                          src={ getImage(cta1.miniatura_botao_cta1, 'xl') }
                          className="bg-image"
                        />
                      </div>
                    </div>
                    <div className="col">
                      <div className="lh-h">
                        <div className="mb-0 font-md-24 fw-600">
                          {cta1.titulo_botao_cta1}
                        </div>
                        <div className="text-muted">
                          {cta1.descricao_botao_cta1}
                        </div>
                      </div>
                    </div>
                    <div className="col-auto">
                      <div className="bg-danger p-32 p-md-40 rounded-6 text-white position-relative overflow-hidden">
                        <i
                          className={`${cta1.icone_botao_cta1} font-32 position-absolute top-50 start-50 translate-middle`}
                        ></i>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              <div className="cta1-ear d-none d-lg-block position-absolute top-0 end-0"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
