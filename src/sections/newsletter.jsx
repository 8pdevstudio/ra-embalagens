export default function Newsletter({ content }) {
  const newsletter = content.detalhes_newsletter

  return (
    <div id="newsletter" className="container-fluid bg-theme-light pt-16">
      <div className="container py-64">
        <div className="text-danger font-md-18 fw-700 mb-16">
          {newsletter.titulo_detalhes_newsletter}
        </div>
        <h2 className="font-title font-32 fw-700 pb-4">
          {newsletter.chamada_detalhes_newsletter}
        </h2>
        <div className="row align-items-end gx-3 mb-16">
          <div className="col-12 col-md">
            <label className="font-md-18 mb-mb-8">Nome</label>
            <input
              type="text"
              className="form-control form-control-lg border-danger rounded-0 p-16"
            />
          </div>
          <div className="col-12 col-md py-24 py-md-0">
            <label className="font-md-18 mb-mb-8">Email</label>
            <input
              type="email"
              className="form-control form-control-lg border-danger rounded-0 p-16"
            />
          </div>
          <div className="col-12 col-md-auto d-grid">
            <button type="button" className="btn btn-danger px-32 py-16">
              <div class="py-1">
                {newsletter.botao_detalhes_newsletter
                  ? newsletter.botao_detalhes_newsletter
                  : 'Assinar'}
              </div>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
