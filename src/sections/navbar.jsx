import Link     from 'next/link'

export default function Navbar({ content }) {

  const config = content.configuracao

  const menu = [
    { id: 1, name: 'sobre nós', href: '/#sobre' },
    { id: 2, name: 'produtos', href: '/#produtos' },
    {
      id: 3,
      name: 'responsabilidade socioambiental',
      href: '/#responsabilidades',
    },
    { id: 4, name: 'blog', href: '/blog' },
  ]

  const links = () => {
    let all = []

    for (const [index, link] of menu.entries()) {
      all.push(
        <li className="nav-item mx-xl-8" key={index}>
          <Link href={ link.href }>
            <a className="nav-link link-dark fw-600 text-uppercase px-8 fs-6">
              {link.name}
            </a>
          </Link>
        </li>
      )
    }

    return all
  }

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  return (
    <div className="container-fluid px-xl-64 py-16 py-xl-24">
      <div className="row align-items-center py-md-8">
        <div className="col">
          <Link href={`/`}>
            <a>
              { getImage(config.logo_configuracao, 'sm') ? (
                <img
                  src={ getImage(config.logo_configuracao, 'sm')}
                  title="content.title"
                  width="120"
                />
              ) : (
                ''
              )}            
            </a>
          </Link>
        </div>
        <div className="d-none d-lg-block col-auto">
          <ul className="nav">{links()}</ul>
        </div>

        <div className="col-auto d-none d-lg-block pl-md-24">
          <Link href={`/#contato`}>
            <a className="btn btn-danger text- px-md-32 py-md-16 text-uppercase font-16">
              contato
            </a>
          </Link>
        </div>

        <div className="col-auto d-block d-lg-none">
          <Link href={`/#contato`}>
            <a className="btn btn-white text-danger px-0 py-md-16 text-uppercase font-16">
              contato
            </a>
          </Link>
        </div>

        <div className="col-auto d-block d-lg-none">
          <Link href={`/#contato`}>
            <a className="btn btn-white text-danger px-16 py-16 text-uppercase font-24">
              <i className="far fa-ellipsis-v"></i>
            </a>
          </Link>
        </div>

      </div>
    </div>
  )
}

