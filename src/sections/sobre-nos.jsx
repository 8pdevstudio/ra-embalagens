import { Swiper, SwiperSlide } from 'swiper/react'
import { Pagination, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

import Helper from '/src/helper.js'

const helper = new Helper()

export default function SobreNos({ content }) {

  const sobre = content.sobre_nos

  // console.log(content)

  const slides = (galeria) => {
    let slides = []

    if (!galeria.length) {
      return slides
    }

    if(sobre.video_sobre_nos){

      const video_id = helper.YoutubeId(sobre.video_sobre_nos)

      slides.push(
        <SwiperSlide className="swiper-slide" key="first">
          <div className="ratio ratio-4x3 rounded-6 overflow-hidden bg-dark">
            <iframe className="bg-image" id={`player${ video_id }`} loading="lazy" width="100%" height="100%" src={`https://www.youtube.com/embed/${ video_id }?loop=1&playlist=${ video_id }&rel=0&showinfo=0&modestbranding=1&theme=dark`} frameBorder="0" allow="accelerometer; autoplay; modestbranding; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div>
        </SwiperSlide>
      )
    }

    for (const [index, gal] of galeria.entries()) {
      slides.push(
        <SwiperSlide className="swiper-slide" key={index}>
          <div className="ratio ratio-4x3 rounded-6 overflow-hidden">
            <img
              loading="lazy"
              src={gal.md}
              className="w-100 h-100 image-cover"
            />
          </div>
        </SwiperSlide>
      )
    }

    return slides
  }

  return (
    <div id="sobre" className="position-relative mt-64 mt-md-144 mb-md-112">
      
      <div className="position-absolute start-0 bottom-0 mb--32 mb-lg--112 py-88 py-md-144 bg-danger w-70"></div>

      <div className="position-relative z-index-1 mb-56">

        <div className="container">
          <div className="position-relative">
            <div className="row">

              <div className="col-lg-6 pb-64 pb-lg-0 pr-md-32 mt--48">
                <div className="sticky-top pt-48">

                  <div className="sobre-nos-ear d-none d-lg-block"></div>

                  <p className="text-danger font-md-18 fw-700 mb-24">
                    {sobre.titulo_sobre_nos}
                  </p>
                  <h2 className="font-title font-32 font-md-64 fw-700 mb-48">
                    {sobre.chamada_sobre_nos}
                  </h2>
                  <div className="">
                    <a
                      v-if="sobre.redirecionamento_sobre_nos"
                      href={sobre.redirecionamento_sobre_nos.text}
                      className="btn btn-danger btn-lg rounded-5 pl-40 pr-112 py-24 position-relative"
                    >
                      {sobre.redirecionamento_sobre_nos.label
                        ? sobre.redirecionamento_sobre_nos.label
                        : 'Saiba mais'}
                      <div className="bg-dark bg-opacity-10 position-absolute top-0 end-0 px-40 d-flex align-items-center h-100">
                        <i className="far fa-chevron-right small position-absolute top-50 start-50 translate-middle me-n1"></i>
                      </div>
                    </a>
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="lh-body font-md-18 fw-300 text-secondary pl-lg-24">
                  <span
                    dangerouslySetInnerHTML={{
                      __html: sobre.texto_sobre_nos,
                    }}
                  ></span>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div className="position-relative w-100 overflow-hidden mt-32 mt-md-112">

          <div className="position-absolute z-index-2 w-100 top-50 start-0 translate-middle-y">
            <div className="container px-24">
              <div className="d-flex align-items-center justify-content-between h-0">
                <button className="slide-sobre-prev btn btn-white text-danger p-2 p-md-3 lh-1 mr-24">
                  <i className="far fa-chevron-left mx-2 my-1 font-md-18"></i>
                </button>
                <button className="slide-sobre-next btn btn-white text-danger p-2 p-md-3 lh-1">
                  <i className="far fa-chevron-right mx-2 my-1 font-md-18"></i>
                </button>                  
              </div>
            </div>
          </div>

          <div className="container">
            <Swiper
                className="swiper swiper-visible"
                pagination={{
                  clickable: true,
                }}
                navigation={{
                  nextEl: '.slide-sobre-next',
                  prevEl: '.slide-sobre-prev',
                }}
                modules={[Pagination, Navigation]}
                spaceBetween={32}
                slidesPerView={1}
                breakpoints={{
                  640: {
                    slidesPerView: 2,                    
                  },
                  1024: {
                    slidesPerView: 3,                    
                  },
                }}
            >
              {slides(sobre.galeria_sobre_nos)}
            </Swiper>
          </div>
        </div>
      </div>

    </div>
  )
}
