import { Swiper, SwiperSlide } from 'swiper/react'
import { Pagination, Navigation } from 'swiper'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'

export default function Depoimentos({ content }) {
  const depoimentos = content.depoimentos

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  const slides = (depoimentos) => {
    let slides = []

    if (!depoimentos.length) {
      return slides
    }

    Object.values(depoimentos).map((depoimento, index) => {
      slides.push(
        <SwiperSlide className="swiper-slide" key={index}>
          <div className="slider-content">
            <div className="px-xl-88">
              <h2 className="font-18 font-md-32 fw-500 mb-0 lh-body">
                {depoimento.mensagem_lista_depoimentos}
              </h2>
              <hr className="max-w-250 my-24 my-md-32 bg-white opacity-100" />
              <h2 className="font-24 fw-500">
                {depoimento.nome_lista_depoimentos}
              </h2>
              <div className="font-16">
                {depoimento.atividade_lista_depoimentos}
              </div>
            </div>
          </div>
        </SwiperSlide>
      )
    })

    return slides
  }
  return (
    <div
      id="depoimentos"
      className="container-fluid position-relative bg-dark text-white overflow-hidden"
    >
      <img className="bg-image" src={ getImage(depoimentos.imagem_fundo_depoimentos, 'lg') } />
      <div className="bg-image bg-dark opacity-75"></div>
      <div className="position-relative row py-64 py-xl-112">
        <div className="col-12 col-xl-6 max-w-900 text-xl-end position-relative z-index-4">

          <div className="position-relative text-start max-w-550 d-inline-block py-24 py-md-48">
            <div className="position-absolute top-0 end-0 w-100 mr-24 mr-xl-0 vw-xl-50 h-100 bg-danger"></div>
            <div className="position-relative">
              <p className="d-none font-md-18 fw-700 mb-24">
                {depoimentos.titulo_depoimentos}
              </p>
              <h3 className="font-title font-32 font-xl-48 fw-700">
                {depoimentos.chamada_depoimentos}
              </h3>
            </div>
          </div>

          <div className="d-flex justify-content-xl-end pt-24">
            <button className="slide-button-prev btn btn-white text-danger p-2 p-xl-3 lh-1 mr-24">
              <i className="far fa-chevron-left mx-2 my-1 font-md-18"></i>
            </button>
            <button className="slide-button-next btn btn-white text-danger p-2 p-xl-3 lh-1">
              <i className="far fa-chevron-right mx-2 my-1 font-md-18"></i>
            </button>
          </div>

        </div>
        <div className="col-12 col-xl-6 pt-56 py-xl-48">
          <Swiper
            className="swiper-visible swiper-fade-out"
            pagination={{
              clickable: true,
            }}
            navigation={{
              nextEl: '.slide-button-next',
              prevEl: '.slide-button-prev',
            }}
            modules={[Pagination, Navigation]}
          >
            {slides(depoimentos.lista_depoimentos)}
          </Swiper>
        </div>
      </div>
    </div>
  )
}
