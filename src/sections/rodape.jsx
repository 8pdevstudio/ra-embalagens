export default function Rodape({ content }) {

  const rodape         = content.contato
        rodape.config  = content.configuracao        

  const menu = [
    { id: 1, name: 'sobre nós', href: '/#sobre' },
    { id: 2, name: 'produtos', href: '/#produtos' },
    {
      id: 3,
      name: 'responsabilidade socioambiental',
      href: '/#responsabilidades',
    },
    { id: 4, name: 'blog', href: '/blog' },
  ]

  const links = (links) => {
    let all = []

    Object.values(links).map((link, key) => {
      all.push(
        <li className="nav-item d-block d-md-inline-block px-16" key={key}>
          <a
            href={link.href}
            className="nav-link link-dark fw-600 text-uppercase fs-6"
          >
            {link.name}
          </a>
        </li>
      )
    })
    return all
  }

  const getImage = (image, size = 'md') => {

    if (image !== null && image !== undefined) {
      return image[0] && image[0][size] ? image[0][size] : null
    }

    return null
  }

  return (
    <div className="container-xxl position-relative overflow-hidden text-center text-xl-start pb-40 pt-64 pt-md-96 px-md-144">
      <div className="row align-items-center justify-content-center">

        <div className="col-12 col-md">
          { getImage(rodape.config.logo_configuracao, 'sm') ? (
            <img
              src={ getImage(rodape.config.logo_configuracao, 'sm')}
              title="Logo"
              className="max-w-200 mb-24 mb-md-32"
            />
          ) : (
            ''
          )}

          <div className="text-secondary max-w-300 mx-auto ms-md-0 mb-32 mb-md-48">
            {rodape.config.descricao_configuracao}
          </div>
        </div>

        <div className="col-12 col-md">
          <div className="max-w-400 mx-auto mb-32 mb-xl-0">
            <div className="row gx-3 mt-16 flex-nowrap text-start">
              <div className="col-auto">
                <div className="">
                  <i className="fa fa-phone text-danger"></i>
                </div>
              </div>
              <div className="col">
                <a
                  className="text-dark"
                  href={`tel:55${rodape.telefone_contato.replace(/\D/g, '')}`}
                >
                  <p className="mb-0">Ligue {rodape.telefone_contato}</p>
                </a>
              </div>
            </div>

            <div className="row gx-3 mt-16 flex-nowrap text-start">
              <div className="col-auto">
                <div className="">
                  <i className="fa fa-envelope text-danger"></i>
                </div>
              </div>
              <div className="col">
                <a
                  className="text-dark"
                  href={`mailto:${rodape.email_contato}`}
                >
                  <p className="mb-0">{rodape.email_contato}</p>
                </a>
              </div>
            </div>

            <div className="row gx-3 mt-16 flex-nowrap text-start">
              <div className="col-auto">
                <div className="">
                  <i className="fa fa-map-marker-alt text-danger"></i>
                </div>
              </div>
              <div className="col">
                <a
                  className="text-dark"
                  href={`https://www.google.com.br/maps/search/${rodape.endereco_contato}`}
                  target="_blank"
                >
                  <p className="mb-0">{rodape.endereco_contato}</p>
                </a>
              </div>
            </div>

          </div>
        </div>

        <div className="col-12 col-md">
          <div className="max-w-400 float-md-end">
            <a
              href={rodape.whatsapp_texto_botao_contato.text}
              target={rodape.whatsapp_texto_botao_contato.tab}
            >
              <div className="bg-light rounded-7 d-inline-block d-md-block p-16 w-100 my-32 mt-md-16 mb-md-24">
                <div className="row gx-3 d-flex flex-nowrap align-items-center">
                  <div className="col">
                    <div className="lh-body">
                      <div className="mb-0 font-md-18 px-16 lh-h fw-600 nowrap d-flex text-dark">
                        {rodape.whatsapp_texto_botao_contato.label}
                      </div>
                    </div>
                  </div>
                  <div className="col-auto">
                    <div className="bg-danger p-40 rounded-6 text-white position-relative overflow-hidden">
                      <i className="fab fa-whatsapp font-32 position-absolute top-50 start-50 translate-middle"></i>
                    </div>
                  </div>
                </div>
              </div>
            </a>

            <div
              className="my-16 mb-xl-0 text-secondary w-100 max-w-350 mx-auto ms-md-0 font-16"
              dangerouslySetInnerHTML={{
                __html: rodape.horario_contato,
              }}
            >
            </div>
          </div>
        </div>
      </div>

      <ul className="nav d-block d-md-flex mt-32 mt-xl-0 mx--16">{links(menu)}</ul>

      <hr className="my-32 my-md-40" />

      <div className="row align-items-center justify-content-between text-center">
        <div className="col-12 col-xl-auto">
          <a href="#" className="link-secondary decoration-none font-16 fw-400">
            Termos / Politica de privacidade
          </a>
        </div>
        <div className="col-12 col-xl-auto">
          <p className="mb-0 text-secondary font-16 py-24 py-xl-0 fw-400">
            © Copyright {new Date().getFullYear()}
          </p>
        </div>
        <div className="col-12 col-xl-auto">

          <div className="d-flex justify-content-center">
            <a
              href={rodape.facebook_contato}
              target="_blank"
              className="border-danger p-4 mx-1 border-1 rounded-circle position-relative border"
            >
              <i className="fab fa-facebook-f text-danger font-24 position-absolute top-50 start-50 translate-middle"></i>
            </a>

            <a
              href={rodape.instagram_contato}
              target="_blank"
              className="border-danger p-4 mx-1 border-1 rounded-circle position-relative border"
            >
              <i className="fab fa-instagram text-danger font-24 position-absolute top-50 start-50 translate-middle"></i>
            </a>

            <a
              href={rodape.linkedin_contato}
              target="_blank"
              className="border-danger p-4 mx-1 border-1 rounded-circle position-relative border"
            >
              <i className="fab fa-linkedin text-danger font-24 position-absolute top-50 start-50 translate-middle"></i>
            </a>

            <a
              href={rodape.twitter_contato}
              target="_blank"
              className="border-danger p-4 mx-1 border-1 rounded-circle position-relative border"
            >
              <i className="fab fa-twitter text-danger font-24 position-absolute top-50 start-50 translate-middle"></i>
            </a>

            <a
              href={rodape.youtube_contato}
              target="_blank"
              className="border-danger p-4 mx-1 border-1 rounded-circle position-relative border"
            >
              <i className="fab fa-youtube text-danger font-24 position-absolute top-50 start-50 translate-middle"></i>
            </a>
          </div>

        </div>
      </div>
    </div>
  )
}
