export default function Numeros({ content }) {
  const numeros = content.numeros

  const listaNumeros = (listaNumeros) => {
    let lista = []

    if (!listaNumeros.length) {
      return lista
    }

    for (const [index, numero] of listaNumeros.entries()) {
      lista.push(
        <div className="col-12 col-md-6 col-lg-4 d-flex pt-64 pl-64 pl-md-16" key={index}>
          <div className="position-relative">
            <div className="p-48 p-md-56 bg-danger position-absolute rounded-circle top-0 start-0 mt--8 ml--40 ml-md--48"></div>
            <div className="position-relative z-index-1">
              <div className={`${ numero.numero_lista_numeros.length > 5 ? 'font-48 font-lg-64' : 'font-64 font-lg-96' } fw-600 lh-h`}>
                {numero.numero_lista_numeros}
              </div>
              <div className={`${ numero.descricao_lista_numeros.length > 32 ? ' max-w-300' : ' max-w-200' } font-16 font-md-24 fw-bold`}>
                {numero.descricao_lista_numeros}
              </div>
            </div>
          </div>
        </div>
      )
    }

    return lista
  }

  return (
    <div id="numeros" className="py-64 position-relative overflow-hidden py-lg-144">
      <div className="container">
        <div className="text-danger text-center font-md-18 fw-700 mb-24">
          {numeros.titulo_numeros}
        </div>
        <div className="row align-items-end justify-content-center text-center gx-5 mb-md-32 pb-md-40">
          <div className="col-lg-9">
            <h2 className="font-title font-32 font-md-64 fw-700 mb-0">
              {numeros.chamada_numeros}
            </h2>
          </div>
          <div className="col-md-12 text-center mt-24 mt-md-32">
            <div className="text-secondary max-w-800 mx-auto font-16 font-md-24 pb-8">
              {numeros.descricao_numeros}
            </div>
          </div>
        </div>
        <div className="row">{listaNumeros(numeros.lista_numeros)}</div>
      </div>
    </div>
  )
}
