class Helper {

	constructor () {}

	YoutubeId(url = false){

		var ID = ''

		if(url){

			url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/)

			if(url[2] !== undefined) {
				ID = url[2].split(/[^0-9a-z_\-]/i)
				ID = ID[0]
			}
			else {
				ID = url
			}
		}

		return ID
	}

	nl2br(str, is_xhtml) {
	    if (typeof str === 'undefined' || str === null) {
	        return ''
	    }
	    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>'
	    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
	}

	clean(str) {
	    if (typeof str !== 'string' || str === null) {
	        return ''
	    }
	    return str.replace(/(<([^>]+)>)/gi, "")
	}

	cleanText(str) {
		return this.nl2br(str.replace(/<\/?span[^>]*>/g,""))
	}

	phoneNumber(str){
		return str.replace(/\D/g, "")
	}

	getExtenseData(data_informada = ''){

		let meses 	= new Array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro")
		let semana 	= new Array("Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado")

		var dia_informado = data_informada.split('-')[2]
		var mes_informado = data_informada.split('-')[1]
		var ano_informado = data_informada.split('-')[0]

		var data = dia_informado.split('T')[0] + ' de ' + meses[parseInt(mes_informado)] + ' de ' + ano_informado

		return data.split('T')[0]
	}
}

export default Helper