import Link     from 'next/link'

import Helper   from '/src/helper'
import Content  from '/src/content'

const helper  = new Helper()
const cont    = new Content()

export default function ListBlog({ content, current = false }) {

  // console.log(content.blog, '<--')

  let blog        = content.blog ? content.blog : []
  let detalhes    = content.detalhes_blog
  let categorias  = content.categorias_blog

  const list = (blog) => {

    let posts = []

    if (!blog.length) {
      return posts
    }

    Object.values(blog).map((post, key) => {

      let cat   = post.categorias_blog ? post.categorias_blog : []
          cat   = cat.length ? cat[0] : {}

      let image = post.imagem_blog[0] ? post.imagem_blog[0] : ''

      posts.push(
        <div key={key} className={`col-12 col-md-${ key < 2 ? '6' : '4' } opacity-in mb-72 mb-md-96`}>
          <Link href={ `/blog/${ post.slug ?? '' }` }>
            <a>
              <div className="ratio ratio-16x9 bg-light rounded-6 overflow-hidden">
                <img loading="lazy" className={`bg-image ${ image ? '' : 'd-none' }`} src={ key < 2 ? image.md : image.sm } />
              </div>
            </a>
          </Link>
          <div className="">
            <div className="text-muted pb-8 pt-16">
              { helper.getExtenseData( post.created_at ) }
            </div>              
            <Link href={ `/blog/${ post.slug ?? '' }` }>
              <a className="text-dark">
                <h5 className={ key < 2 ? 'font-24 font-md-32 pr-md-24 font-title fw-700 mb-8' : 'font-24 font-title fw-700' }>
                  { post.titulo_blog } 
                </h5>
              </a>
            </Link>
            <div className="pr-md-24 opacity-in-target mt-16">
              <Link href={ `/blog/${ post.slug ?? '' }` }>
                <a className="fw-500 text-danger border-bottom border-2 border-danger">
                  Acessar
                </a>
              </Link>
            </div>
          </div>
        </div>
      )
    })

    return posts
  }

  return (
      <section className="container-fluid position-relative overflow-hidden">
        <div className="bg-theme-light pt-lg-56">
          <div className="container pb-144 pt-96">
            <div className="position-relative">
              <div className="sobre-nos-ear"></div>
            </div>
            <div className="row align-items-end mb-md-16">
              <div className="col">
                <div className="max-w-900">
                  <div className="text-danger font-md-18 fw-700 mb-24">
                    { detalhes.titulo_detalhes_blog }
                  </div>
                  <h2 className="font-title font-32 font-md-56 mb-0 fw-700">
                    { detalhes.chamada_detalhes_blog }
                  </h2>
                </div>
              </div>
              <div className="col-12 max-w-450">
                <div className="row align-items-center">
                  <div className="col-12 col-md-auto">
                    <div className="text-danger font-md-18 fw-700">Categorias</div>
                  </div>
                  <div className="col">
                    <div className="dropdown">

                      <div data-bs-toggle="dropdown" className="rounded-5 border border-danger overflow-hidden w-100 d-flex position-relative">
                        <div className="w-100 bg-white py-16 px-24">
                          Todos
                        </div>
                        <button className="small btn btn-danger rounded-0 px-24 h-auto h-100">
                          <i className="far fa-chevron-down small"></i>
                        </button>
                      </div>

                      <ul className="dropdown-menu w-100 border-0 rounded-6 px-16 py-8 dropdown-menu-end">
                        {
                          categorias.map((categoria, key) => {
                            return (
                              <li key={key}>
                                <Link href={`/blog/categoria/${ categoria.slug }`}>
                                  <a className="text-dark">
                                    <div className="bg-white rounded-2 p-8 my-8">
                                      { categoria.titulo_categorias_blog }
                                    </div>
                                  </a>
                                </Link>
                              </li>
                            )
                          })
                        }
                      </ul>

                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container mt--64 mt--88 pb-md-32 position-relative z-index-1">
          <div className="">
            <div className="row">

              {list(blog)}

            </div>
          </div>
        </div>
      </section>
  )
}