class Content {

	constructor () {}

	getContent(item, col){
		return (item[col] !== null && item[col] !== undefined) ? item : null
	}

	getImage(image, size = 'md'){

		if (image !== null && image !== undefined) {

			if(image[0]){

				if(image[0][size]){
					return image[0][size]					
				}
				else{
					return image[0]['default'] ? image[0]['default'] : null	
				}
			}
			else if(image['default']){
				return image['default']
			}			
		}

		return null
	}

	fixAbsolutePathImage(src = ''){

		if(src.search('/var/www/html/') > -1){
			src = src.replace('/var/www/html/8pdev.studio/api/storage/app/public/', 'https://api.8pdev.studio/storage/')
			src = src.split('uploads')[0]
		}

		return src
	}

	getBlogImage(content, size){

		let sizes 	= []
		let details = JSON.parse(content.preview[0].details)
		let path 	= details.absolute_path

		// console.log(details.sizes, '<')

		let find 	= false
		let valid 	= ''

		details.sizes.forEach((item, key) => {

			if(find == false){
				valid = path+item.path
			}

			find = (item.size == size) ? true : find
		})

		return valid
	}
}

export default Content;