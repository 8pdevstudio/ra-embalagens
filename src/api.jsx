import axios from 'axios';

class Api {
	constructor () {}
	call (requestType, url, data = {}, opts = {}) {

		data['token'] = process.env.TOKEN

		return new Promise((resolve, reject) => {
			axios[requestType]( process.env.API_REST + url, data, opts)
				.then(response => {
					resolve(response)
				})
			.catch(({response}) => {
				if ([400, 401, 418].indexOf(response.status) > -1) {	               
					console.log('error')
					return false
				}

				resolve(response)
			});
		});
	}
}

export default Api;