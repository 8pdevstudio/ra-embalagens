import Head from 'next/head'
import Navbar from '/src/sections/navbar'
import Rodape from '/src/sections/rodape'

export default function Template({
  children,
  pageTitle,
  description,
  ...props
}) {

  return (
    <>
      <Head>
        <title>{pageTitle} - RA Embalagens</title>
        <meta name="robots" content="follow, index" />
        <meta http-equiv="cache-control" content="public" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta property="og:site_name" content="RA Embalagens" />
        <meta property="og:description" content="" />
        <meta property="og:title" content={`${ pageTitle } - RA Embalagens`} />
        <meta property="og:image" content="" />
        <script dangerouslySetInnerHTML={{ __html: `if(new URLSearchParams(window.location.search).get('cache') == 'true') window.location.href = window.location.href.split('?')[0];`}}></script>
      </Head>

      <Navbar current={props.current} content={props.content} />

      <main>{children}</main>

      <Rodape content={ props.content } />
    </>
  )
}
