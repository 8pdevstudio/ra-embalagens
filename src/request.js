var request = {
				sobre_nos: {},
				produtos: {},
				trabalhe_conosco: {},
				newsletter: {},
				detalhes_newsletter: {},
				depoimentos: {},
				contato: {},
				lgpd_scripts: {},
				configuracao: {}
			}			

class Request {

	constructor () {}

	responseJSON(req = []){		

		return JSON.stringify({...req, ...request})
	}

	// BLOG
		BlogProps(){

			let response  = {
								blog: {
									filter: {
										where: "visibiliade_post_blog=publico"
									}
								},
								detalhes_blog: [],
								categorias_blog: []
							} 

			return this.responseJSON(response)
		}

		BlogCategoriaPaths(){
			return this.responseJSON({categorias_blog: []})
		}

		BlogCategoriaProps(slug){			

			let response  = {
								blog: {
									related: [slug],
									filter: {
										where: "visibiliade_post_blog=publico"
									}
								},
								detalhes_blog: [],
								categorias_blog: []
							}

			return this.responseJSON(response)
		}

		PostPaths(){

			let response  = {
								blog: {
									filter: {
										where: "visibiliade_post_blog=publico"
									}
								}
							}

			return this.responseJSON(response)
		}

		PostProps(slug){

			let response = {
								blog:{
									filter: {
										where: `slug=${slug}`,
									}
								},
								related: {
									from: `blog`,
									limit: 3,
									filter: {
										where: `slug!=${slug}`,
										where: "visibiliade_post_blog=publico"
									}
								}
	                       	}

			return this.responseJSON(response)
		}
	// ---

	// FEEDBACK
	FeedbackProps(){
		return this.responseJSON()
	}
	// ---
}

export default Request;